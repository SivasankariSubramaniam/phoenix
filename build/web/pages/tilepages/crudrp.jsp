<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
        <fmt:setBundle basename="ApplicationResources" />
        <title> 
            <fmt:message key="title.crudrp"/> 
        </title>
    </head>
    <body>
        <c:if test="${param['searchrs_setupps'] != 'true'}">
            <h1><fmt:message key="label.crudrp"/></h1>
        </c:if>
        <c:if test="${param['searchrs_setupps'] == 'true'}">
            <h1><fmt:message key="label.crudrp.select"/></h1>
        </c:if>
            
        <c:url var="url" scope="page" value="/nocturne/addeditrp">
            <c:param name="name" value=""/>
            <c:param name="description" value=""/>
            <c:param name="duration" value=""/>
            <c:param name="insert" value="true"/>
        </c:url>
        <c:if test="${param['searchrs_setupps'] != 'true'}">
            <a href="${url}"><fmt:message key="label.crudrp.add"/></a>
            <br/><br/>
        </c:if>
        
        <table class="borderAll">
            <tr>
                <th><fmt:message key="label.crudrp.name"/></th>
                <th><fmt:message key="label.crudrp.description"/></th>
                <th><fmt:message key="label.crudrp.duration"/></th>
                <th>
                    <c:if test="${param['searchrs_setupps'] == 'true'}">
                        <fmt:message key="label.crudrp.select"/>
                    </c:if>
                    <c:if test="${param['searchrs_setupps'] != 'true'}">
                        <fmt:message key="label.crudrp.editdelete"/>
                    </c:if>
                </th>
            </tr>
            <c:forEach var="crudrp" items="${rps}" varStatus="status">
                <tr class="${status.index%2==0?'even':'odd'}">
                    <td class="nowrap">${crudrp.name}</td>
                    <td class="nowrap">${crudrp.description}</td>
                    <td class="nowrap">${crudrp.typicalDuration}</td>
                    <td class="nowrap">

                        <c:if test="${param['searchrs_setupps'] == 'true'}">
                            <%-- naw start --%>
                            <c:url var="selurl" scope="page" value="/nocturne/addeditPrgSch">
                                <c:param name="insert" value="${param['insert']}"/>  
                                <c:param name="dateOfProgram" value="${param['dateOfProgram']}"/>
                                <c:param name="startTime" value="${param['startTime']}"/>
                                <c:param name="presenterID" value="${param['presenterID']}"/>
                                <c:param name="producerID" value="${param['producerID']}"/>
                                <c:param name="programName" value="${crudrp.name}"/>     
                                <c:param name="typicalDuration" value="${crudrp.typicalDuration}"/>
                            </c:url>
                            <%-- naw end --%>
                            <a href="${selurl}"><fmt:message key="label.select"/></a>
                        </c:if>
                        <c:if test="${param['searchrs_setupps'] != 'true'}">
                            <c:url var="updurl" scope="page" value="/nocturne/addeditrp">
                                <c:param name="name" value="${crudrp.name}"/>
                                <c:param name="description" value="${crudrp.description}"/>
                                <c:param name="typicalDuration" value="${crudrp.typicalDuration}"/>
                                <c:param name="insert" value="false"/>
                            </c:url>
                            <a href="${updurl}"><fmt:message key="label.edit"/></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <c:url var="delurl" scope="page" value="/nocturne/deleterp">
                                <c:param name="name" value="${crudrp.name}"/>
                            </c:url>
                            <a href="${delurl}"><fmt:message key="label.delete"/></a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>