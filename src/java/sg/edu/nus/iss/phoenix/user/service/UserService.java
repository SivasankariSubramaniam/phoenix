/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;

/**
 *
 * @author sivasankarisubramaniam
 */
public class UserService {
    
    DAOFactoryImpl factory;
    UserDao userdao;

    /**
     *
     */
    public UserService() {
        super();
	// TODO Auto-generated constructor stub
	factory = new DAOFactoryImpl();
	userdao = factory.getUserDAO();
    }
    
    /**
     * This method will create a new user 
     * @param user user to be created
     */
    public void processCreate(User user) {
	try {
            userdao.create(user);
	} catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
	}
    }

    /**
     * This method will modify the data of the user
     * @param user  user to be modified
     */
    public void processModify(User user) {
		
        try {
            userdao.save(user);
        }catch (NotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
	} catch (SQLException e) {
            // TODO Auto-generated catch block
	}
    }

    /**
     * This method is used to delete a user based on Id. Throws NotFoundException if user not found  and SQLException incase of database error
     * @param id id of user to be deleted
     */
    public void processDelete(String id) {

            try {
                User user = new User(id);
                userdao.delete(user);
            } catch (NotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }
    
    /**
     * This method is used to load all users. Throws NotFoundException if user not found  and SQLException incase of database error
     * @return  list of all users
     */
    public List<User> loadAll() {
            List<User> data = null;
            try {
                data = userdao.loadAll();
            } catch (SQLException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return data; 
	}
    
    /**
     *  This method is used to retrive specific user based on ID. Throws NotFoundException if user not found  and SQLException incase of database error
     * @param userWithId user object with ID
     * @return user object will full information
     */
    public User loadUser(User userWithId) {
            User data = null;
            try {
                data = userdao.load(userWithId);
            } catch (SQLException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
            return data; 
	}
    
    /**
     *  This method is used to set the password of  specific user to default("1234") based on ID. Throws NotFoundException if user not found  and SQLException incase of database error
     * @param id id of user to reset password
     */
    public void processResetPassword(String id) {

            try {
                User user = new User(id);
                userdao.resetPassword(user);
            } catch (NotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }
    
    
}
