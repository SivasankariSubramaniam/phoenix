/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.delegate;

import java.util.List;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author sivasankarisubramaniam,vikram
 */
public class UserDelegate {
    
    /**
     *
     * @return
     */
    public List<User> loadAll() {
        UserService service = new UserService();
	List<User> users = service.loadAll();
        return users;
    }
    
    /**
     *
     * @return
     */
    public User load(User user) {
        UserService service = new UserService();
	User returnedUser = service.loadUser(user);
        return returnedUser;
    }
    
    /**
     *
     * @param user
     */
    public void processCreate(User user) {
	UserService service = new UserService();
	service.processCreate(user);
		
    }
    
    /**
     *
     * @param user
     */
    public void processModify(User user) {
        UserService service = new UserService();
        service.processModify(user);
		
    }

    /**
     *
     * @param id
     */
    public void processDelete(String id) {
	UserService service = new UserService();
	service.processDelete(id);
    }
    
    /**
     *
     * @param id
     */
    public void processresetPassword(String id) {
	UserService service = new UserService();
	service.processResetPassword(id);
    }
    
    /**
     *
     * @param user
     * @return
     */
    public boolean checkUserExists(User user) {
        UserService service = new UserService();
	User returnedUser = service.loadUser(user);
        if(returnedUser == null) {
            return false;
        } else {
            return true;
        }
    }
}
