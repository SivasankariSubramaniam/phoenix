/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.user.delegate.ReviewSelectPresentorProducerDelegate;
import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;

/**
 *
 * @author Vikram
 */
@Action("selectPresProd")
public class SelectPresenterProducerCmd implements Perform {
    
    /**
     * This method will return the JSP for the corresponding Action called by the UI
     * @param string
     * @param hsr
     * @param hsr1
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String perform(String string, HttpServletRequest hsr, HttpServletResponse hsr1) throws IOException, ServletException {
        ReviewSelectPresentorProducerDelegate  rsppd=new ReviewSelectPresentorProducerDelegate();
        List<Presenter>  dataPresnter=rsppd.reviewSelectPresenter(); 
        List<Producer> dataProducer=rsppd.reviewSelectProducer();
        hsr.setAttribute("presenters", dataPresnter);
        hsr.setAttribute("producers", dataProducer);
        return "/pages/selectPresenter.jsp";
    }
    
}
