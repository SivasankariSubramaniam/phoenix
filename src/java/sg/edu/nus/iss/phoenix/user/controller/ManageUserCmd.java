/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.Role;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author sivasankarisubramaniam,Vikram
 */
@Action("manageUser")
public class ManageUserCmd implements Perform {
    
    /**
     * This method will return the JSP for the corresponding Action called by the UI
     * @param string
     * @param hsr
     * @param hsr1
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String perform(String string, HttpServletRequest hsr, HttpServletResponse hsr1) throws IOException, ServletException {
        User currentUser = (User) hsr.getSession().getAttribute("user");
        UserService service = new UserService();
        List<User> data;
        
        if("viewProfile".equals(hsr.getParameter("param"))) {
            data = new ArrayList<User>();
            data.add(currentUser);
            hsr.setAttribute("users", data);
            hsr.setAttribute("view", "true");
        } else {
            data = service.loadAll();
            hsr.setAttribute("users", data);
            hsr.setAttribute("view", "false");
        }
        return "/pages/userMaintenance.jsp";
    }
    
}
