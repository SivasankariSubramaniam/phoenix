/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.delegate;

import java.util.List;
import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;
import sg.edu.nus.iss.phoenix.user.service.ReviewSelectPresentorService;
import sg.edu.nus.iss.phoenix.user.service.ReviewSelectProducerService;

/**
 *
 * @author Vikram
 */

public class ReviewSelectPresentorProducerDelegate {
    
    private ReviewSelectPresentorService servicepresenter;
     private ReviewSelectProducerService serviceproducer;
    
    /**
     *
     */
    public ReviewSelectPresentorProducerDelegate() {
            super();
            servicepresenter = new ReviewSelectPresentorService();
            serviceproducer = new ReviewSelectProducerService();
    }
    
    /**
     *This methods retrives list of all user where their role is Presenter
     * @return
     */
    public List<Presenter> reviewSelectPresenter() {       
        return servicepresenter.loadAll();
    }
     
    /**
     * This methods retrives list of all user where their role is Producer
     * @return
     */
    public List<Producer> reviewSelectProducer() {       
        return serviceproducer.loadAll();
    } 
   
}
