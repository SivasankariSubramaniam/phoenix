/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.PresenterDao;
import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;


/**
 *
 * @author Vikram
 */
public class ReviewSelectPresentorService {
    
    DAOFactoryImpl factory;
    PresenterDao presenterDao;
    
    /**
     *
     */
    public ReviewSelectPresentorService() {
        super();
	// TODO Auto-generated constructor stub
	factory = new DAOFactoryImpl();
	presenterDao = factory.getPresenterDAO();
    }
    
    /**
     * This method is used to load all Presenters
     * @return list of all presenters
     */
    public List<Presenter> loadAll() {
            List<Presenter> data = null;
            try {
                data = presenterDao.loadAll();
            } catch (SQLException ex) {
                Logger.getLogger(ReviewSelectPresentorService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return data; 
	}
     
     
    
}
