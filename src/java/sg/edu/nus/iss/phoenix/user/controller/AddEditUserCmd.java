/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.Role;
import sg.edu.nus.iss.phoenix.role.service.RoleService;

/**
 *
 * @author sivasankarisubramaniam
 */
@Action("addeditUser")
public class AddEditUserCmd implements Perform {
    
    /**
     * This method will return the JSP for the corresponding Action called by the UI
     * @param string
     * @param hsr
     * @param hsr1
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String perform(String string, HttpServletRequest hsr, HttpServletResponse hsr1) throws IOException, ServletException {
        RoleService roleService = new RoleService();
        List<Role> roleData = roleService.loadAll();
        hsr.setAttribute("roles", roleData);
        StringBuilder builder = new StringBuilder();
        if(hsr.getParameterValues("userRole") != null && "false".equals(hsr.getParameter("insert"))) {
            for(String userRole: hsr.getParameterValues("userRole")) {
                if (builder.length() != 0) {
                    builder.append(" ");
                }
                builder.append(userRole);
            }
            // set role
            hsr.setAttribute("userRoles", builder.toString());
        }
        return "/pages/setupUser.jsp";
    }
    
}
