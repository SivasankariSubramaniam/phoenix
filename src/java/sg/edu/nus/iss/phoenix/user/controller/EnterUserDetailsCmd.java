/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.Role;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.role.service.RoleService;
import sg.edu.nus.iss.phoenix.user.delegate.UserDelegate;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author sivasankarisubramaniam
 */
@Action("enterUserDetails")
public class EnterUserDetailsCmd implements Perform {
    
    // list to contain errors
    List<String> errors = new ArrayList<String>();
    
    UserDelegate del = new UserDelegate();
    RoleService roleService = new RoleService();
    List<Role> roles = new ArrayList<Role>();

    /**
     * This method will return the JSP for the corresponding Action called by the UI
     * @param string
     * @param hsr
     * @param hsr1
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String perform(String string, HttpServletRequest hsr, HttpServletResponse hsr1) throws IOException, ServletException {
        
        // check if all fields are entered
        if("true".equals(hsr.getParameter("ins"))) {
            if(hsr.getParameter("name") == null || hsr.getParameter("password") == null
                    || hsr.getParameter("id") == null
                    || hsr.getParameter("confirmPassword") == null 
                    || (hsr.getParameterValues("userRole") == null)) {
                errors.add("Please enter all fields.");
            }
            // check if the passwords entered match
            if(hsr.getParameter("password") != null && hsr.getParameter("confirmPassword") != null) {
                if(!hsr.getParameter("confirmPassword").equals(hsr.getParameter("password"))) {
                    errors.add("Passwords do not match. Please re-enter your passord.");
                }
            }
        } else {
            if(hsr.getParameterValues("userRole") == null) {
                errors.add("Please enter all fields.");
            }
        }
        
        // if no errors, proceed to persist create or update of user
        if(errors.isEmpty()) {
            
            
            roles = roleService.loadAll();
            
            User user = new User();
            user.setName(hsr.getParameter("name"));
            user.setId(hsr.getParameter("id"));
            user.setPassword(hsr.getParameter("password"));
            // append roles using colon (:)
            StringBuilder builder = new StringBuilder();
            String [] userSelectedRoles = null;
            userSelectedRoles = hsr.getParameterValues("userRole");
            
            for(Role role: roles) {
                for(String roleString: userSelectedRoles) {
                    if(role.getRole().equals(roleString)) {
                        if (builder.length() != 0) {
                            builder.append(":");
                        }
                        builder.append(role.getRole());
                    }
                }
                
            }
            // set role
            user.getRoles().add(new Role(builder.toString()));
            String ins = (String) hsr.getParameter("insert");
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Insert Flag: " + ins);
            if (ins.equalsIgnoreCase("true")) {
                if(!del.checkUserExists(user)) {
                    del.processCreate(user);
                } else {
                    errors.add("User already exists.");
                    // if got errors, inform user and show setup page
                    setErrorsToHTTPRequest(hsr);
                    
                }
                    
            } else {
                    del.processModify(user);
            }

            UserService service = new UserService();
            List<User> data = service.loadAll();
            hsr.setAttribute("users", data);
            return "/pages/userMaintenance.jsp";
        } else {
            // if got errors, inform user and show setup page
            setErrorsToHTTPRequest(hsr);

            return "/pages/setupUser.jsp";
        }
    }
    
    private void setErrorsToHTTPRequest(HttpServletRequest request) {
        // if got errors, inform user and show setup page
        request.setAttribute("errors", errors);

        List<Role> roleData = roleService.loadAll();

        request.setAttribute("roles", roleData);
        
    }
    
}