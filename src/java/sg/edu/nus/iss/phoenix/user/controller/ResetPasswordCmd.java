/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.delegate.UserDelegate;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author vikram
 */
@Action("resetPassword")
public class ResetPasswordCmd implements Perform {
    
    /**
     * This method will return the JSP for the corresponding Action called by the UI
     * @param path
     * @param req
     * @param resp
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        UserDelegate del = new UserDelegate();
        String id = req.getParameter("id");
        del.processresetPassword(id);

        UserService service = new UserService();
        List<User> data = service.loadAll();
        req.setAttribute("users", data);
        return "/pages/userMaintenance.jsp?msg=true";

    }
}
