/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.ProducerDao;
import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;


/**
 *
 * @author Vikram
 */
public class ReviewSelectProducerService {
    
    DAOFactoryImpl factory;
    ProducerDao producerDao;
    
    /**
     *
     */
    public ReviewSelectProducerService() {
        super();
	// TODO Auto-generated constructor stub
	factory = new DAOFactoryImpl();
	producerDao = factory.getProducerDAO();
    }
    
    /**
     * This method is used to load all Producers. 
     * @return list of all Producers
     */
    public List<Producer> loadAll() {
            List<Producer> data = null;
            try {
                data = producerDao.loadAll();
            } catch (SQLException ex) {
                Logger.getLogger(ReviewSelectPresentorService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return data; 
	}
     
     
    
}
