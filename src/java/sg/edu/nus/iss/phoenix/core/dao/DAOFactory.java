/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.core.dao;

import sg.edu.nus.iss.phoenix.authenticate.dao.PresenterDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.ProducerDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.RoleDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.radioprogram.dao.ProgramDAO;
import sg.edu.nus.iss.phoenix.programschedule.dao.ScheduleDAO;

/**
 *
 * @author projects
 */
public interface DAOFactory {

    /**
     *
     * @return ProgramDAO
     */
    ProgramDAO getProgramDAO();

    /**
     *
     * @return RoleDAO
     */
    RoleDao getRoleDAO();

    /**
     *
     * @return UserDAO
     */
    UserDao getUserDAO();
	
    /**
     *
     * @return ScheduleDAO
     */
    ScheduleDAO getScheduleDAO();
        
     /**
     *
     * @return PresenterDAO
     */
    PresenterDao getPresenterDAO();
    
    /**
     *
     * @return ProducerDAO
     */ 
    ProducerDao getProducerDAO();
    
}
