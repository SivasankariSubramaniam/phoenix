package sg.edu.nus.iss.phoenix.core.dao;

import sg.edu.nus.iss.phoenix.authenticate.dao.PresenterDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.ProducerDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.RoleDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;
import sg.edu.nus.iss.phoenix.authenticate.dao.impl.PresenterDaoImpl;
import sg.edu.nus.iss.phoenix.authenticate.dao.impl.ProducerDaoImpl;
import sg.edu.nus.iss.phoenix.authenticate.dao.impl.RoleDaoImpl;
import sg.edu.nus.iss.phoenix.authenticate.dao.impl.UserDaoImpl;
import sg.edu.nus.iss.phoenix.radioprogram.dao.ProgramDAO;
import sg.edu.nus.iss.phoenix.radioprogram.dao.impl.ProgramDAOImpl;
import sg.edu.nus.iss.phoenix.programschedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAOImpl;

/**
 *
 * @author sivasankarisubramaniam
 */
public class DAOFactoryImpl implements DAOFactory {
	private UserDao userDAO = new UserDaoImpl();
	private RoleDao roleDAO = new RoleDaoImpl();
	private ProgramDAO rpdao = new ProgramDAOImpl();
        private ScheduleDAO prgSchdao = new ScheduleDAOImpl();
        private PresenterDao presenterdao = new PresenterDaoImpl();
        private ProducerDao producerdao = new ProducerDaoImpl();

	@Override
	public UserDao getUserDAO() {
		// TODO Auto-generated method stub
		return userDAO;
	}

	@Override
	public RoleDao getRoleDAO() {
		// TODO Auto-generated method stub
		return roleDAO;
	}

	@Override
	public ProgramDAO getProgramDAO() {
		// TODO Auto-generated method stub
		return rpdao;
	}
        
        @Override
	public ScheduleDAO getScheduleDAO() {
		// TODO Auto-generated method stub
		return prgSchdao;
	}
        
        @Override
        public PresenterDao getPresenterDAO() {
            return presenterdao;
        }
        
        @Override
        public ProducerDao getProducerDAO() {
            return producerdao;
        }
        
}
