/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.radioprogram.delegate.ProgramDelegate;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;

/**
 *
 * @author naw; Virendra;
 */
public class WeeklySchedule {

    /**
     *
     */
    public class Cell {

        private ProgramSlot programSlot;
        private int row;
        private int column;

        /**
         *
         * @param programSlot
         * @param row
         * @param column
         */
        public Cell(ProgramSlot programSlot, int row, int column) {
            this.programSlot = programSlot;
            this.row = row;
            this.column = column;
        }

        /**
         *
         * @return
         */
        public boolean isEmpty() {
            return programSlot == null;
        }

        /**
         *
         * @return
         */
        public boolean isView() {
            //return WeeklySchedule.this.row == row && WeeklySchedule.this.column == column;
            return true;
        }

        /**
         *
         * @return
         */
        public ProgramSlot getProgramSlot() {
            return programSlot;
        }

    }

    private Calendar startDate;
    private Map<Date, ProgramSlot> programSlots = new HashMap<Date, ProgramSlot>();
    private List<Date> cannotAddDates = new ArrayList<Date>();
    private int row;
    private int column;
    private int viewingStartMinute;

    /**
     *
     */
    public WeeklySchedule() {
        setStartDate(null);
    }

    /**
     *
     * @param i
     * @return
     */
    public String getDay(int i) {
        switch (i) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
        }
        return "";
    }

    /**
     *
     * @param i
     * @return
     */
    public String getDate(int i) {
        Calendar calender = (Calendar) startDate.clone();
        calender.add(Calendar.DAY_OF_MONTH, i);
        return String.format("%d-%02d-%02d", calender.get(Calendar.YEAR), (1 + calender.get(Calendar.MONTH)), calender.get(Calendar.DAY_OF_MONTH));
    }

    /**
     *
     * @param date
     */
    public void setStartDate(String date) {

        Calendar instance = Calendar.getInstance();
        if (date != null) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                instance.setTime(dateFormat.parse(date));
            } catch (ParseException ex) {
                Logger.getLogger(WeeklySchedule.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            instance.set(Calendar.HOUR_OF_DAY, 0);
            instance.set(Calendar.MINUTE, 0);
            instance.set(Calendar.SECOND, 0);
            instance.set(Calendar.MILLISECOND, 0);
        }

        final int day = instance.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case Calendar.SUNDAY:
                break;
            case Calendar.MONDAY:
                instance.add(Calendar.DAY_OF_MONTH, -1);
                break;
            case Calendar.TUESDAY:
                instance.add(Calendar.DAY_OF_MONTH, -2);
                break;
            case Calendar.WEDNESDAY:
                instance.add(Calendar.DAY_OF_MONTH, -3);
                break;
            case Calendar.THURSDAY:
                instance.add(Calendar.DAY_OF_MONTH, -4);
                break;
            case Calendar.FRIDAY:
                instance.add(Calendar.DAY_OF_MONTH, -5);
                break;
            case Calendar.SATURDAY:
                instance.add(Calendar.DAY_OF_MONTH, -6);
                break;

        }
        startDate = instance;
    }

    /**
     *
     * @return
     */
    public String getStartDate() {
        return String.format("%d-%02d-%02d", startDate.get(Calendar.YEAR), (1 + startDate.get(Calendar.MONTH)), startDate.get(Calendar.DAY_OF_MONTH));
    }

    /**
     *
     * @return
     */
    public Date getStart() {
        return startDate.getTime();
    }

    /**
     *
     * @return
     */
    public Date getEnd() {
        Calendar endDate = (Calendar) startDate.clone();
        endDate.add(Calendar.DAY_OF_MONTH, 7);
        return endDate.getTime();
    }

    /**
     *
     * @return
     */
    public String getEndDate() {
        Calendar endDate = (Calendar) startDate.clone();
        endDate.add(Calendar.DAY_OF_MONTH, 7);
        return String.format("%d-%02d-%02d", endDate.get(Calendar.YEAR), (1 + endDate.get(Calendar.MONTH)), endDate.get(Calendar.DAY_OF_MONTH));
    }

    /**
     *
     * @param time
     * @return
     */
    public String getTime(int time) {
        return String.format("%02d:%02d", time / 60, time % 60);
    }

    /**
     *
     * @param i
     * @param j
     * @return
     */
    public Cell getCell(int i, int j) {
        Calendar select = (Calendar) startDate.clone();
        select.add(Calendar.DAY_OF_MONTH, j);
        select.add(Calendar.MINUTE, i);
        Date d = select.getTime();
        final ProgramSlot program = programSlots.get(select.getTime());
        return new Cell(program, i, j);
    }

    public boolean canAdd(int i, int j) {
        Calendar select = (Calendar) startDate.clone();
        select.add(Calendar.DAY_OF_MONTH, j);
        select.add(Calendar.MINUTE, i);
        return !cannotAddDates.contains(select.getTime());
    }

    /**
     *
     * @param programSlots
     */
    public void setProgramSlots(List<ProgramSlot> programSlots) {
        this.programSlots.clear();
        cannotAddDates.clear();
        RadioProgram radioProgram = new RadioProgram();
        ProgramDelegate del = new ProgramDelegate();

        for (ProgramSlot ps : programSlots) {

            radioProgram = del.findRP(ps.getRadioProgram().getName());
            Date startTimeOfProgram = combineDateTime(ps.getDateOfProgram(), ps.getStartTime());
            ps.setStartTime(startTimeOfProgram);
            ps.setRadioProgram(radioProgram);

            Calendar gc = Calendar.getInstance();
            gc.setTime(startTimeOfProgram);
            gc.add(Calendar.HOUR, ps.getRpDuration().getHours());
            gc.add(Calendar.MINUTE, ps.getRpDuration().getMinutes());
            Date endTimeOfProgram = gc.getTime();

            this.programSlots.put(startTimeOfProgram, ps);
            Calendar i = Calendar.getInstance();
            i.setTime(ps.getStartTime());

            Calendar end = Calendar.getInstance();
            end.setTime(endTimeOfProgram);
            for (; i.before(end); i.add(Calendar.MINUTE, getTimeIntervalMinute())) {
                final Date date = (Date) i.getTime().clone();
                cannotAddDates.add(date);
            }
        }
    }

    private Date combineDateTime(Date date, Date time) {
        Calendar calendarA = Calendar.getInstance();
        calendarA.setTime(date);
        Calendar calendarB = Calendar.getInstance();
        calendarB.setTime(time);

        calendarA.set(Calendar.HOUR_OF_DAY, calendarB.get(Calendar.HOUR_OF_DAY));
        calendarA.set(Calendar.MINUTE, calendarB.get(Calendar.MINUTE));
        calendarA.set(Calendar.SECOND, calendarB.get(Calendar.SECOND));
        calendarA.set(Calendar.MILLISECOND, calendarB.get(Calendar.MILLISECOND));

        Date result = calendarA.getTime();
        return result;
    }

    /**
     *
     * @param row
     * @param column
     */
    public void setView(String row, String column) {
        this.row = -1;
        this.column = -1;
        if (row != null) {
            this.row = Integer.valueOf(row);
        }
        if (column != null) {
            this.column = Integer.valueOf(column);
        }
    }

    public int getTimeIntervalMinute() {
        return 30;
    }

    public int getShowIntevalMinute() {
        return 1440;
    }

    public int getViewingStartMinute() {
        return viewingStartMinute;
    }

    public void setViewingStartMinute(String viewingStartMinute) {
        this.viewingStartMinute = 0;

        if (viewingStartMinute != null) {
            this.viewingStartMinute = Integer.valueOf(viewingStartMinute);
        }
    }
}
