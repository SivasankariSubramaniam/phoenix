/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;

/**
 *
 * @author gandhis ; naw; Virendra;
 */
public class ProgramSlot implements Cloneable, Serializable {

    /**
     * eclipse identifier
     */
    private static final long serialVersionUID = -5500218812568593553L;

    /**
     * Persistent Instance variables. This data is directly mapped to the
     * columns of database table.
     */
    private Date dateOfProgram;
    private String presenterID;
    private String producerID;
    private RadioProgram radioProgram;
    private Date startTime;
    private String assingedBy;
    private Date rpDuration;

    /**
     * Constructors. The first one takes no arguments and provides the most
     * simple way to create object instance. The another one takes one argument,
     * which is the primary key of the corresponding table.
     */
    public ProgramSlot() {

    }

    /**
     *
     * @param dateOfProgram
     */
    public ProgramSlot(Date dateOfProgram) {

        this.dateOfProgram = dateOfProgram;
    }

    /**
     * Get- and Set-methods for persistent variables. The default behaviour does
     * not make any checks against malformed data, so these might require some
     * manual additions.
     *
     * @return
     */
    public Date getDateOfProgram() {
        return this.dateOfProgram;
    }

    /**
     *
     * @param dateOfProgram
     */
    public void setDateOfProgram(Date dateOfProgram) {
        this.dateOfProgram = dateOfProgram;
    }

    /**
     *
     * @return
     */
    public String getPresenterID() {
        return this.presenterID;
    }

    /**
     *
     * @param presenterID
     */
    public void setPresenterID(String presenterID) {
        this.presenterID = presenterID;
    }

    /**
     *
     * @return
     */
    public String getProducerID() {
        return this.producerID;
    }

    /**
     *
     * @param producerID
     */
    public void setProducerID(String producerID) {
        this.producerID = producerID;
    }

    /**
     *
     * @return
     */
    public RadioProgram getRadioProgram() {
        return this.radioProgram;
    }

    /**
     *
     * @param radioProgram
     */
    public void setRadioProgram(RadioProgram radioProgram) {
        this.radioProgram = radioProgram;
    }

    /**
     *
     * @return
     */
    public Date getStartTime() {
        return this.startTime;
    }

    /**
     *
     * @param startTime
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     */
    public Date getRpDuration() {
        return this.rpDuration;
    }

    /**
     *
     * @param rpDuration
     */
    public void setRpDuration(Date rpDuration) {
        this.rpDuration = rpDuration;
    }

    /**
     *
     * @return
     */
    public String getAssingedBy() {
        return this.assingedBy;
    }

    /**
     *
     * @param assingedBy
     */
    public void setAssingedBy(String assingedBy) {
        this.assingedBy = assingedBy;
    }

    /**
     * setAll allows to set all persistent variables in one method call. This is
     * useful, when all data is available and it is needed to set the initial
     * state of this object. Note that this method will directly modify instance
     * variables, without going trough the individual set-methods.
     *
     * @param dateOfProgram
     * @param presenterID
     * @param radioProgram
     * @param producerID
     * @param startTime
     * @param assingedBy
     * @param rpDuration
     */
    public void setAll(Date dateOfProgram,
            String presenterID,
            String producerID,
            RadioProgram radioProgram,
            Date startTime,
            String assingedBy,
            Date rpDuration) {
        this.dateOfProgram = dateOfProgram;
        this.presenterID = presenterID;
        this.producerID = producerID;
        this.radioProgram = radioProgram;
        this.startTime = startTime;
        this.assingedBy = assingedBy;
        this.rpDuration = rpDuration;
    }
}
