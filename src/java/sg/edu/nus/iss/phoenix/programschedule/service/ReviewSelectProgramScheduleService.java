/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;
import sg.edu.nus.iss.phoenix.programschedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;

/**
 *
 * @author gandhis; Virendra;
 */
public class ReviewSelectProgramScheduleService {

    DAOFactoryImpl factory;
    ScheduleDAO scheduleProgramdao;

    /**
     *
     */
    public ReviewSelectProgramScheduleService() {
        super();
        // TODO Auto-generated constructor stub
        factory = new DAOFactoryImpl();
        scheduleProgramdao = factory.getScheduleDAO();
    }

    /**
     *
     * @return the list of all scheduled program slots
     */
    public List<ProgramSlot> reviewSelectProgramSchedule() {
        List<ProgramSlot> data = null;
        try {
            data = scheduleProgramdao.loadAll();
        } catch (SQLException ex) {
            Logger.getLogger(ReviewSelectProgramScheduleService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

    /**
     *
     * @param from
     * @param until
     * @return the list of all scheduled program slots of corresponding period
     */
    public List<ProgramSlot> searchSchedules(Date from, Date until) {
        return scheduleProgramdao.searchMatching(from, until);
    }

    /**
     *
     * @return a Map containing Schedule details and its corresponding Radio prg
     * duration
     */
    public Map<Integer, ArrayList<String>> loadSchSlot() {
        Map<Integer, ArrayList<String>> data = null;
        try {
            data = scheduleProgramdao.loadSchDtls();
        } catch (SQLException e) {
            Logger.getLogger(ReviewSelectProgramScheduleService.class.getName()).log(Level.SEVERE, null, e);
        }
        return data;
    }
}
