/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;
import sg.edu.nus.iss.phoenix.user.delegate.ReviewSelectPresentorProducerDelegate;

/**
 *
 * @author gandhis; Virendra;
 */
@Action("copyPrgSch")
public class CopyScheduledProgramCmd implements Perform {

    /**
     *
     * @param path
     * @param req
     * @param resp
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        ReviewSelectPresentorProducerDelegate rsppd = new ReviewSelectPresentorProducerDelegate();
        List<Presenter> dataPresnter = rsppd.reviewSelectPresenter();
        List<Producer> dataProducer = rsppd.reviewSelectProducer();
        req.setAttribute("presenters", dataPresnter);
        req.setAttribute("producers", dataProducer);

        return "/pages/setupPrgSch.jsp";
    }
}
