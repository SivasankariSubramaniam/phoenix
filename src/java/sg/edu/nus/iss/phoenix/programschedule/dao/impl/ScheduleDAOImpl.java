/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.core.dao.DBConstants;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.programschedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;

/**
 *
 * @author gandhis; naw; Virendra;
 */
public class ScheduleDAOImpl implements ScheduleDAO {

    Connection connection;

    /* (non-Javadoc)
     * @see sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAO#createValueObject()
     */
    @Override
    public ProgramSlot createValueObject() {
        return new ProgramSlot();
    }

    /* (non-Javadoc)
     * @see sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAO#getObject(java.util.Date, java.util.Date)
     */
    @Override
    public ProgramSlot getObject(Date dateOfProgram, Date startTime) throws NotFoundException, SQLException {

        ProgramSlot valueObject = createValueObject();
        valueObject.setDateOfProgram(dateOfProgram);
        valueObject.setStartTime(startTime);
        load(valueObject);
        return valueObject;
    }

    /* (non-Javadoc)
     * @see sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAO#load(sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot)
     */
    @Override
    public void load(ProgramSlot valueObject) throws NotFoundException,
            SQLException {

        if (valueObject.getDateOfProgram() == null) {
            throw new NotFoundException("Can not select without Primary-Key!");
        }
        String sql = "SELECT * FROM `program-slot` WHERE (`startTime` = ?  and `dateOfProgram` = ? )";
        PreparedStatement stmt = null;
        openConnection();
        try {
            stmt = connection.prepareStatement(sql);
            stmt.setTime(1, new Time(valueObject.getStartTime().getTime()));
            stmt.setTimestamp(2, new Timestamp(valueObject.getDateOfProgram().getTime()));
            singleQuery(stmt, valueObject);

        } catch (ParseException ex) {
            Logger.getLogger(ScheduleDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
    }

    /* (non-Javadoc)
     * @see sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAO#save(sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot)
     */
    public void save(ProgramSlot valueObject) throws NotFoundException, SQLException {

        String sql = "UPDATE `program-slot` SET `presenterId` = ?,`producerId` = ?,`program-name` = ?, `rpDuration` = ? WHERE (`startTime` = ?  and `dateOfProgram` = ? )";
        PreparedStatement stmt = null;
        openConnection();
        try {
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, valueObject.getPresenterID());
            stmt.setString(2, valueObject.getProducerID());
            stmt.setString(3, valueObject.getRadioProgram().getName());

            SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
            stmt.setString(4, sdf2.format(valueObject.getRpDuration().getTime()));

            stmt.setTime(5, new Time(valueObject.getStartTime().getTime()));
            stmt.setTimestamp(6, new Timestamp(valueObject.getDateOfProgram().getTime()));

            int rowcount = databaseUpdate(stmt);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
    }

    /* (non-Javadoc)
     * @see sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAO#delete(sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot)
     */
    @Override
    public void delete(Date dateOfProgram, Time startTime) throws NotFoundException,
            SQLException, ParseException {

        if (dateOfProgram == null) {
            throw new NotFoundException("Can not delete without Primary-Key!");
        }

        String sql = "DELETE FROM `program-slot` WHERE (`dateOfProgram` = ?  and `startTime` = ?); ";
        PreparedStatement stmt = null;
        openConnection();
        try {
            stmt = connection.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(dateOfProgram.getTime()));
            stmt.setTime(2, new Time(startTime.getTime()));
            int rowcount = databaseUpdate(stmt);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
    }

    /**
     * databaseUpdate-method. This method is a helper method for internal use.
     * It will execute all database handling that will change the information in
     * tables. SELECT queries will not be executed here however. The return
     * value indicates how many rows were affected. This method will also make
     * sure that if cache is used, it will reset when data changes.
     *
     * @param stmt This parameter contains the SQL statement to be excuted.
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     * statements or (2) 0 for SQL statements that return nothing
     * @throws java.sql.SQLException
     */
    protected int databaseUpdate(PreparedStatement stmt) throws SQLException {

        int result = stmt.executeUpdate();

        return result;
    }

    private void openConnection()  {
        try {
            Class.forName(DBConstants.COM_MYSQL_JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            if (connection == null) {
                this.connection = DriverManager.getConnection(DBConstants.dbUrl,DBConstants.dbUserName, DBConstants.dbPassword);
            } else 
                if (connection.isClosed()) 
                    this.connection = DriverManager.getConnection(DBConstants.dbUrl,DBConstants.dbUserName, DBConstants.dbPassword);
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void closeConnection() {
        try {
            if (connection != null) {
                this.connection.close();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * databaseQuery-method. This method is a helper method for internal use. It
     * will execute all database queries that will return only one row. The
     * resultset will be converted to valueObject. If no rows were found,
     * NotFoundException will be thrown.
     *
     * @param stmt This parameter contains the SQL statement to be excuted.
     * @param valueObject Class-instance where resulting data will be stored.
     * @throws sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException
     * @throws java.sql.SQLException
     */
    protected void singleQuery(PreparedStatement stmt, ProgramSlot valueObject)
            throws NotFoundException, SQLException, ParseException {

        ResultSet result = null;
        openConnection();
        try {
            result = stmt.executeQuery();

            if (result.next()) {
                valueObject.setPresenterID(result.getString("presenterID"));
                valueObject.setProducerID(result.getString("producerID"));
                valueObject.setStartTime(result.getTime("startTime"));
                valueObject.setDateOfProgram(result.getDate("dateOfProgram"));
                valueObject.setRadioProgram(new RadioProgram(result.getString("program-name")));
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                Date rpDuration = sdf.parse(result.getString("rpDuration"));
                valueObject.setRpDuration(rpDuration);
            } else {
                // System.out.println("RadioProgram Object Not Found!");
                throw new NotFoundException("RadioProgram Object Not Found!");
            }
        } finally {
            if (result != null) {
                result.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
    }

    /* (non-Javadoc)
     * @see sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAO#loadAll()
     */
    @Override
    public List<ProgramSlot> loadAll() throws SQLException {
        
        String sql = "SELECT * FROM `program-slot` ORDER BY `program-name` ASC; ";
        List<ProgramSlot> searchResults = null;
        try {
            openConnection();
            searchResults = listQuery(connection.prepareStatement(sql));
        } catch (ParseException ex) {           
        } finally {
            closeConnection();
        }
        // System.out.println("record size"+searchResults.size());
        return searchResults;
    }

    /* (non-Javadoc)
     * @see sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAO#loadSchDtls()
     */
    @Override
    public Map<Integer, ArrayList<String>> loadSchDtls() throws SQLException {
        Map<Integer, ArrayList<String>> searchResults;
        searchResults = null;
        try {
            if (connection == null) {
                openConnection();
            }

            String sql = "SELECT dateOfProgram,startTime,typicalDuration FROM `radio-program` , `program-slot` "
                    + "WHERE `program-name` = `name` ORDER BY `dateOfProgram`,`startTime` ASC";

            searchResults = listSchSlot(connection.prepareStatement(sql));
        } catch (ParseException ex) {
            Logger.getLogger(ScheduleDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                closeConnection();
            }
        }

        // System.out.println("record size"+searchResults.size());
        return searchResults;
    }

    /* (non-Javadoc)
     * @see sg.edu.nus.iss.phoenix.programschedule.dao.impl.ScheduleDAO#create(sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot)
     */
    @Override
    public synchronized void create(ProgramSlot valueObject)
            throws SQLException {

        String sql = "";
        PreparedStatement stmt = null;
        openConnection();
        try {
            sql = "INSERT INTO `program-slot` (`dateOfProgram`, `startTime`,`program-name`,`producerId`,`presenterId`,`assingedBy`,`rpDuration`) VALUES (?,?,?,?,?,?,?); ";
            stmt = connection.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(valueObject.getDateOfProgram().getTime()));
            stmt.setTime(2, new Time(valueObject.getStartTime().getTime()));
            stmt.setString(3, valueObject.getRadioProgram().getName());
            stmt.setString(4, valueObject.getProducerID());
            stmt.setString(5, valueObject.getPresenterID());
            stmt.setString(6, valueObject.getAssingedBy());
            SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
            stmt.setString(7, sdf2.format(valueObject.getRpDuration().getTime()));
            int rowcount = databaseUpdate(stmt);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
    }

    /**
     * databaseQuery-method. This method is a helper method for internal use. It
     * will execute all database queries that will return multiple rows. The
     * resultset will be converted to the List of valueObjects. If no rows were
     * found, an empty List will be returned.
     *
     * @param stmt This parameter contains the SQL statement to be excuted.
     * @return list of program slots
     * @throws java.sql.SQLException
     * @throws java.text.ParseException
     */
    protected List<ProgramSlot> listQuery(PreparedStatement stmt) throws SQLException, ParseException {

        ArrayList<ProgramSlot> searchResults = new ArrayList<>();
        ResultSet result = null;
        try {
            result = stmt.executeQuery();

            while (result.next()) {
                ProgramSlot temp = createValueObject();
                temp.setPresenterID(result.getString("presenterID"));
                temp.setProducerID(result.getString("producerID"));
                temp.setStartTime(result.getTime("startTime"));
                temp.setDateOfProgram(result.getDate("dateOfProgram"));
                temp.setRadioProgram(new RadioProgram(result.getString("program-name")));
                SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
                Date rpDuration = sdf2.parse(result.getString("rpDuration"));
                temp.setRpDuration(rpDuration);

                searchResults.add(temp);
            }

        } finally {
            if (result != null) {
                result.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            closeConnection();
        }
        return (List<ProgramSlot>) searchResults;
    }

    /**
     *
     * @param stmt statement object to be used to retrieve the scheduled
     * programs
     * @return list of scheduled program slot info
     * @throws SQLException
     * @throws ParseException
     */
    protected Map<Integer, ArrayList<String>> listSchSlot(PreparedStatement stmt) throws SQLException, ParseException {

        Map<Integer, ArrayList<String>> searchResults = new TreeMap<Integer, ArrayList<String>>();
        ResultSet result = null;

        try {
            if (connection == null) {
                openConnection();
            }
            result = stmt.executeQuery();
            int i = 0;
            while (result.next()) {
                ArrayList<String> temp = new ArrayList<>();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat format2 = new SimpleDateFormat("HH:mm:ss");
                temp.add(format1.format(result.getDate("dateOfProgram")));
                temp.add(format2.format(result.getTime("startTime")));
                temp.add(format2.format(result.getTime("typicalDuration")));
                searchResults.put(i, temp);
                i++;
            }

        } finally {
            if (result != null) {
                result.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (connection != null) {
                closeConnection();
            }
        }
        return (Map<Integer, ArrayList<String>>) searchResults;
    }

    /**
     *
     * programs of the corresponding period
     * @return list of scheduled program slot info
     */
    public List<ProgramSlot> searchMatching(Date from, Date until) {

        List<ProgramSlot> searchResults = null;
        try {
            if (connection == null) {
                openConnection();
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            String sql = "SELECT * FROM `program-slot` WHERE (dateOfProgram >= '"
                    + dateFormat.format(from) + "' ) AND (dateOfProgram < '" + dateFormat.format(until) + "' ) ORDER BY 'dateOfProgram' ASC ";

            try {
                searchResults = listQuery(connection.prepareStatement(sql));
            } catch (SQLException ex) {
                Logger.getLogger(ScheduleDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ScheduleDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception e) {

        } finally {
            if (connection != null) {
                closeConnection();
            }
        }
        return searchResults;
    }
}
