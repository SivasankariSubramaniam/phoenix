/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.service;

import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.programschedule.dao.ScheduleDAO;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;

/**
 *
 * @author gandhis; naw;
 */
public class ScheduleService {

    DAOFactoryImpl factory;
    ScheduleDAO prgSchdao;

    /**
     *
     */
    public ScheduleService() {
        super();
        // TODO Auto-generated constructor stub
        factory = new DAOFactoryImpl();
        prgSchdao = factory.getScheduleDAO();
    }

    /**
     *
     * @param rp program slot to be created
     */
    public void processCreate(ProgramSlot rp) {
        try {
            prgSchdao.create(rp);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     *
     * @param rp modified Program slot to be saved
     */
    public void processModify(ProgramSlot rp) {
        try {
            prgSchdao.save(rp);
        } catch (NotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(" in process modify " + e.getMessage());
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(" in process modify " + e.getMessage());
        }
    }

    /**
     *
     * @param dateOfProgram date of program schedule
     * @param startTime time of the program schedule
     */
    public void processDelete(Date dateOfProgram, Time startTime) {

        try {
            prgSchdao.delete(dateOfProgram, startTime);
        } catch (NotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     *
     * @param from from date of the schedules
     * @param until end date of the schedules
     * @return list of program slots within the specified date range
     */
    public List<ProgramSlot> searchSchedules(Date from, Date until) {

        return prgSchdao.searchMatching(from, until);
    }

    public List<ProgramSlot> loadAll() throws SQLException {

        return prgSchdao.loadAll();
    }

}
