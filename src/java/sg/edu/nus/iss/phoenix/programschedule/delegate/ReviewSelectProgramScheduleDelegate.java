/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.delegate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.programschedule.entity.WeeklySchedule;
import sg.edu.nus.iss.phoenix.programschedule.service.ReviewSelectProgramScheduleService;

/**
 *
 * @author gandhis; Virendra;
 */
public class ReviewSelectProgramScheduleDelegate {

    private ReviewSelectProgramScheduleService service;
    public WeeklySchedule weeklySchedule;

    /**
     *
     */
    public ReviewSelectProgramScheduleDelegate() {
        service = new ReviewSelectProgramScheduleService();
        weeklySchedule = new WeeklySchedule();
    }

    /**
     *
     * @return
     */
    public List<ProgramSlot> reviewSelectProgramSchedule() {
        return service.searchSchedules(weeklySchedule.getStart(), weeklySchedule.getEnd());
    }

    /**
     *
     * @return
     */
    public Map<Integer, ArrayList<String>> loadSchSlot() {
        Map<Integer, ArrayList<String>> data = service.loadSchSlot();
        return data;
    }

    /**
     *
     * @return
     */
    public WeeklySchedule WeeklySchedule() {
        return weeklySchedule;
    }
}
