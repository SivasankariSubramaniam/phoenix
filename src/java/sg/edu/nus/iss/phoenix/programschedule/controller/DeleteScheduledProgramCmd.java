/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.Time;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.programschedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.programschedule.delegate.ReviewSelectProgramScheduleDelegate;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author naw; Virendra;
 */
@Action("deletePrgSch")
public class DeleteScheduledProgramCmd implements Perform {

    /**
     *
     * @param path
     * @param req
     * @param resp
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        ScheduleDelegate del = new ScheduleDelegate();
        try {
            Date dateOfProgram = new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter("dateOfProgram"));
            Date startTime = new SimpleDateFormat("HH:mm").parse(req.getParameter("startTime"));
            del.processDelete(dateOfProgram, new Time(startTime.getTime()));
        } catch (ParseException ex) {
            Logger.getLogger(DeleteScheduledProgramCmd.class.getName()).log(Level.SEVERE, null, ex);
        }

        ReviewSelectProgramScheduleDelegate delegate = new ReviewSelectProgramScheduleDelegate();
        delegate.WeeklySchedule().setStartDate(req.getParameter("weeklystartDate"));
        List<ProgramSlot> programSlots = delegate.reviewSelectProgramSchedule();
        delegate.WeeklySchedule().setProgramSlots(programSlots);
        delegate.WeeklySchedule().setView(req.getParameter("row"), req.getParameter("column"));
        delegate.WeeklySchedule().setViewingStartMinute(req.getParameter("viewingStart"));
        req.setAttribute("weekView", delegate.WeeklySchedule());
        
        return "/pages/weeklyPrgSch.jsp";
    }
}
