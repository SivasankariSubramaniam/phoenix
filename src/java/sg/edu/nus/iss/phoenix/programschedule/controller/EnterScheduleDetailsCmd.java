/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.programschedule.delegate.ScheduleDelegate;
import sg.edu.nus.iss.phoenix.programschedule.delegate.ReviewSelectProgramScheduleDelegate;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;

import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.user.delegate.ReviewSelectPresentorProducerDelegate;

/**
 *
 * @author Naw; Virendra;
 */
@Action("enterPrgSch")
public class EnterScheduleDetailsCmd implements Perform {

    /**
     *
     * @param path
     * @param req
     * @param resp
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        ScheduleDelegate del = new ScheduleDelegate();
        ProgramSlot programSlot = new ProgramSlot();

        // List of errors
        List<String> errors = new ArrayList<String>();
        // Check if any input field is empty
        if (req.getParameter("dateOfProgram") == null || req.getParameter("startTime") == null
                || req.getParameter("presenterID") == null || req.getParameter("programName") == null || req.getParameter("producerID") == null
                || req.getParameter("dateOfProgram").equals("") || req.getParameter("startTime").equals("-- Select Time --")
                || req.getParameter("presenterID").trim().equals("-- Select Presenter --") || req.getParameter("programName").equals("") || req.getParameter("producerID").trim().equals("-- Select Producer --")) {
            errors.add("Please enter all fields.");
        }

        if (errors.isEmpty()) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
                Date dateOfProgram = sdf.parse(req.getParameter("dateOfProgram"));
                Date startTime = sdf2.parse(req.getParameter("startTime") + ":00");
                Date rpDuration = sdf2.parse(req.getParameter("typicalDuration") + ":00");
                programSlot.setDateOfProgram(dateOfProgram);
                programSlot.setStartTime(startTime);
                programSlot.setRpDuration(rpDuration);
            } catch (ParseException ex) {
                Logger.getLogger(EnterScheduleDetailsCmd.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (req.getParameter("presenterID") != null && !req.getParameter("presenterID").equals("")) {
                User presenter = new User(req.getParameter("presenterID"));
                programSlot.setPresenterID(presenter.getId());
            }

            if (req.getParameter("producerID") != null && !req.getParameter("producerID").equals("")) {
                User producer = new User(req.getParameter("producerID"));
                programSlot.setProducerID(producer.getId());
            }
            if (req.getParameter("programName") != null && !req.getParameter("programName").equals("")) {
                RadioProgram radioProgram = new RadioProgram(req.getParameter("programName"));
                programSlot.setRadioProgram(radioProgram);
            }

            User user = (User) req.getSession().getAttribute("user");
            programSlot.setAssingedBy(user.getId());

            String ins = (String) req.getParameter("insert");
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Insert Flag: " + ins);

            //if (validateDuplicatePrgSch()) {
            if (ins.equalsIgnoreCase("true")) {
                try {
                    if (validateDuplicatePrgSchs(programSlot)) {
                        del.processCreate(programSlot);
                    } else {
                        Logger.getLogger(getClass().getName()).log(Level.WARNING, "Failed to create/update Schedule Program slot: ");
                        errors.add("Duplicate Program slot");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EnterScheduleDetailsCmd.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                del.processModify(programSlot);
            }
        }
        
        if (!errors.isEmpty()) {
            // if got errors, disply error message at setup page
            ReviewSelectPresentorProducerDelegate rsppd = new ReviewSelectPresentorProducerDelegate();
            List<Presenter> dataPresnter = rsppd.reviewSelectPresenter();
            List<Producer> dataProducer = rsppd.reviewSelectProducer();
            req.setAttribute("presenters", dataPresnter);
            req.setAttribute("producers", dataProducer);
            req.setAttribute("errors", errors);
            return "/pages/setupPrgSch.jsp";
        } else {
            ReviewSelectProgramScheduleDelegate delegate = new ReviewSelectProgramScheduleDelegate();
            delegate.WeeklySchedule().setStartDate(req.getParameter("weeklystartDate"));
            List<ProgramSlot> programSlots = delegate.reviewSelectProgramSchedule();
            delegate.WeeklySchedule().setProgramSlots(programSlots);
            delegate.WeeklySchedule().setView(req.getParameter("row"), req.getParameter("column"));
            delegate.WeeklySchedule().setViewingStartMinute(req.getParameter("viewingStart"));
            req.setAttribute("weekView", delegate.WeeklySchedule());
            return "/pages/weeklyPrgSch.jsp";
        }

    }

    /**
     *
     * @return
     */
    public boolean validateDuplicatePrgSch() {
        boolean status;
        Map<Integer, Map<String, int[][]>> testData = prepareTestData();
        ReviewSelectProgramScheduleDelegate rsdel = new ReviewSelectProgramScheduleDelegate();
        Map<Integer, ArrayList<String>> dbData = rsdel.loadSchSlot();
        status = validate(dbData, testData);
        return status;
    }

    public boolean validateDuplicatePrgSchs(ProgramSlot currentSlot) throws SQLException {
        boolean status = true;

        ScheduleDelegate del = new ScheduleDelegate();
        List<ProgramSlot> programSlots = del.loadAll();
        for (ProgramSlot ps : programSlots) {
            if (ps.getStartTime().equals(currentSlot.getStartTime()) && ps.getDateOfProgram().equals(currentSlot.getDateOfProgram())) {
                return status = false;
            } else {
                Date psStart = combineDateTime(ps.getDateOfProgram(), ps.getStartTime());
                Date psEnd = addDateTime(psStart, ps.getRpDuration());

                Date currentDateTime = combineDateTime(currentSlot.getDateOfProgram(), currentSlot.getStartTime());
                //currentDateTime = addDateTime(currentDateTime, currentSlot.getRpDuration());

                if (currentDateTime.before(psEnd) && (currentDateTime.after(psStart))) {
                    return false;
                }
            }
        }
        return status;
    }

    private Date combineDateTime(Date date, Date time) {
        Calendar calendarA = Calendar.getInstance();
        calendarA.setTime(date);
        Calendar calendarB = Calendar.getInstance();
        calendarB.setTime(time);

        calendarA.set(Calendar.HOUR_OF_DAY, calendarB.get(Calendar.HOUR_OF_DAY));
        calendarA.set(Calendar.MINUTE, calendarB.get(Calendar.MINUTE));
        calendarA.set(Calendar.SECOND, calendarB.get(Calendar.SECOND));
        calendarA.set(Calendar.MILLISECOND, calendarB.get(Calendar.MILLISECOND));

        Date result = calendarA.getTime();
        return result;
    }

    private Date addDateTime(Date date, Date time) {
        Calendar calendarA = Calendar.getInstance();
        calendarA.setTime(date);
        Calendar calendarB = Calendar.getInstance();
        calendarB.setTime(time);

        calendarA.add(Calendar.HOUR_OF_DAY, calendarB.get(Calendar.HOUR_OF_DAY));
        calendarA.add(Calendar.MINUTE, calendarB.get(Calendar.MINUTE));
        calendarA.add(Calendar.SECOND, calendarB.get(Calendar.SECOND));
        calendarA.add(Calendar.MILLISECOND, calendarB.get(Calendar.MILLISECOND));

        Date result = calendarA.getTime();
        return result;
    }

    private Map<Integer, Map<String, int[][]>> prepareTestData() {
        Map<Integer, Map<String, int[][]>> yrSch = new TreeMap<Integer, Map<String, int[][]>>();
        Map<String, int[][]> testData = new TreeMap<String, int[][]>();
        int[][] daysSlots = new int[7][48];
        for (int k = 1; k <= 52; k++) { // iterate over weeks
            for (int j = 0; j < 7; j++) { // iterate over days
                for (int slot = 0; slot < 48; slot++) { // iterate over program slot
                    daysSlots[j][slot] = 0;
                }
            }
            testData.put("wk schedule" + k, daysSlots);
            yrSch.put(k, testData);
        }
        return yrSch;
    }

    private boolean validate(Map<Integer, ArrayList<String>> dbData, Map<Integer, Map<String, int[][]>> testdata) {
        boolean status = false;
        if (!dbData.isEmpty()) {
            for (Map.Entry<Integer, ArrayList<String>> calendar : dbData.entrySet()) {
                ArrayList<String> dataLst = calendar.getValue();
                String date = dataLst.get(0);
                String timestampStr = dataLst.get(1);
                String dur = dataLst.get(2);
                Logger.getLogger(getClass().getName()).log(Level.INFO, "date" + date + " " + timestampStr + " " + dur);
                try {
                    status = chkAvailability(date, timestampStr, dur, testdata);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } else {
            status = true;
        }
        return status;
    }

    private boolean chkAvailability(String date, String timestampStr, String dur, Map<Integer, Map<String, int[][]>> yrSch) throws ParseException {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "chkAvailability");
        Map<String, int[][]> wkSlot;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Date dt1 = format1.parse(date);
        Calendar c = Calendar.getInstance();
        c.setTime(dt1);
        int weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
        int year = c.get(Calendar.YEAR);
        int day = c.get(Calendar.DAY_OF_WEEK);
        String[] tokens = timestampStr.split(":");
        int hours = Integer.parseInt(tokens[0]);
        int minutes = Integer.parseInt(tokens[1]);
        int stDuration = 2 * hours;
        if (minutes == 30) {
            stDuration = stDuration + 1;
        }
        stDuration = stDuration + 1;
        boolean status = true;
        //int duration = (int) (Float.parseFloat(dur) / 0.5);
        String[] tokensDur = dur.split(":");
        int hoursDur = Integer.parseInt(tokensDur[0]);
        int minutesDur = Integer.parseInt(tokensDur[1]);
        int duration = 2 * hoursDur;
        if (minutesDur == 30) {
            duration = hoursDur + 1;
        }
        if (year == 2016) {
            wkSlot = yrSch.get(weekOfYear);
            int[][] dailySlot = wkSlot.get("wk schedule" + weekOfYear);
            for (int i = 0; i < duration; i++) {
                int availableStatus = dailySlot[day - 1][stDuration + i];
                if (availableStatus != 0) {
                    status = false;
                    break;
                }
            }

            if (status == true) {
                for (int i = 0; i < duration; i++) {
                    dailySlot[day - 1][stDuration + i] = 1;
                }
                wkSlot.put("wk schedule" + weekOfYear, dailySlot);
                yrSch.put(weekOfYear, wkSlot);
            }
        }
        return status;
    }
}
