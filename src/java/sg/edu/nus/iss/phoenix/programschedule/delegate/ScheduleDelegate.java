/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.delegate;

import java.sql.SQLException;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.programschedule.service.ScheduleService;

/**
 *
 * @author gandhis; naw; Virendra;
 */
public class ScheduleDelegate {

    /**
     *
     * @param rp
     */
    public void processCreate(ProgramSlot rp) {
        ScheduleService service = new ScheduleService();
        service.processCreate(rp);		
    }
	
    /**
     *
     * @param rp
     */
    public void processModify(ProgramSlot rp) {
	ScheduleService service = new ScheduleService();
	service.processModify(rp);
    }

    /**
     *
     * @param dateOfProgram
     * @param startTime
     */
    public void processDelete(Date dateOfProgram, Time startTime) {
	ScheduleService service = new ScheduleService();
	service.processDelete(dateOfProgram,startTime);
    }    

      /**
     *

     */
     public List<ProgramSlot> loadAll() throws SQLException {
	ScheduleService service = new ScheduleService();
	return service.loadAll();
    }    
}
