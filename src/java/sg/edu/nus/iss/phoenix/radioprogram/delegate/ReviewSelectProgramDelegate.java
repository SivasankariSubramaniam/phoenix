package sg.edu.nus.iss.phoenix.radioprogram.delegate;

import java.util.List;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;
import sg.edu.nus.iss.phoenix.radioprogram.service.ReviewSelectProgramService;

/**
 *
 * @author sivasankarisubramaniam
 */
public class ReviewSelectProgramDelegate {
    private ReviewSelectProgramService service;
    
    /**
     *
     */
    public ReviewSelectProgramDelegate() {
		service = new ReviewSelectProgramService();
	}
	
    /**
     *
     * @return list of all scheduled programs
     */
    public List<RadioProgram> reviewSelectRadioProgram() {
		return service.reviewSelectRadioProgram();	
	}

}
