package sg.edu.nus.iss.phoenix.radioprogram.entity;

/**
 *
 * @author sivasankarisubramaniam
 */
public class RPSearchObject {
	  private String name;
	    private String description;

    /**
     *
     */
    public RPSearchObject() {
			super();
		}

    /**
     *
     * @param name  name of radio program
     * @param description description of radio program
     */
    public RPSearchObject(String name, String description) {
			super();
			this.name = name;
			this.description = description;
		}

    /**
     *
     * @return name of radio program
     */
    public String getName() {
			return name;
		}

    /**
     *
     * @param name name of radio program
     */
    public void setName(String name) {
			this.name = name;
		}

    /**
     *
     * @return description of radio program
     */
    public String getDescription() {
			return description;
		}

    /**
     *
     * @param description description of radio program
     */
    public void setDescription(String description) {
			this.description = description;
		}
}
