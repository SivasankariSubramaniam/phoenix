/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.role.service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.RoleDao;
import sg.edu.nus.iss.phoenix.authenticate.entity.Role;
import sg.edu.nus.iss.phoenix.core.dao.DAOFactoryImpl;

/**
 *
 * @author sivasankarisubramaniam
 */
public class RoleService {
    DAOFactoryImpl factory;
    RoleDao roledao;

    /**
     *
     */
    public RoleService() {
        super();
	// TODO Auto-generated constructor stub
	factory = new DAOFactoryImpl();
	roledao = factory.getRoleDAO();
    }
    
    /**
     *
     * @return
     */
    public List<Role> loadAll() {
            List<Role> data = null;
            try {
                data = roledao.loadAll();
            } catch (SQLException ex) {
                Logger.getLogger(RoleService.class.getName()).log(Level.SEVERE, null, ex);
            }
            return data; 
	}
}
