/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.authenticate.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.PresenterDao;
import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
import static sg.edu.nus.iss.phoenix.core.dao.DBConstants.COM_MYSQL_JDBC_DRIVER;
import static sg.edu.nus.iss.phoenix.core.dao.DBConstants.dbPassword;
import static sg.edu.nus.iss.phoenix.core.dao.DBConstants.dbUrl;
import static sg.edu.nus.iss.phoenix.core.dao.DBConstants.dbUserName;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;

/**
 *
 * @author Vikram
 */
public class PresenterDaoImpl implements PresenterDao {

    private static final Logger logger = Logger.getLogger(RoleDaoImpl.class.getName());

    Connection connection;

    /**
     *
     */
    public PresenterDaoImpl() {
        super();
        // TODO Auto-generated constructor stub
        connection = openConnection();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * sg.edu.nus.iss.phoenix.authenticate.dao.impl.UserDao#createValueObject()
     */
    @Override
    public Presenter createValueObject() {
        return new Presenter();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * sg.edu.nus.iss.phoenix.authenticate.dao.impl.UserDao#getObject(java.sql
     * .Connection, int)
     */
    @Override
    public Presenter getObject(String id) throws NotFoundException, SQLException {

        Presenter valueObject = createValueObject();
        valueObject.setId(id);
        //load(valueObject);
        return valueObject;
    }

    @Override
    public List<Presenter> loadAll() throws SQLException {

        List<Presenter> searchResults = null;
        try {
            if (connection == null) {
                connection = openConnection();
            }
            String sql = "SELECT * FROM user where role like '%presenter%' ORDER BY id ASC ";
            searchResults = listQuery(this.connection.prepareStatement(sql));

        } catch (Exception e) {

        } finally {
            if (connection != null) {
                closeConnection();
            }
        }
        return searchResults;
    }

    /**
     * databaseQuery-method. This method is a helper method for internal use. It
     * will execute all database queries that will return multiple rows. The
     * resultset will be converted to the List of valueObjects. If no rows were
     * found, an empty List will be returned.
     *
     * @param stmt This parameter contains the SQL statement to be excuted.
     * @return
     * @throws java.sql.SQLException
     */
    protected List<Presenter> listQuery(PreparedStatement stmt) throws SQLException {

        ArrayList<Presenter> searchResults = new ArrayList<>();
        try (ResultSet result = stmt.executeQuery()) {

            while (result.next()) {
                Presenter temp = createValueObject();
                temp.setId(result.getString("id"));
                temp.setName(result.getString("name"));
                searchResults.add(temp);
            }

        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return (List<Presenter>) searchResults;
    }

    private Connection openConnection() {
        Connection conn = null;
        try {
            Class.forName(COM_MYSQL_JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
        }

        try {
            if (connection == null) {
                conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            } else {
                if (connection.isClosed()) 
                    conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            }
        } catch (SQLException e) {
        }
        return conn;
    }

    private void closeConnection() {
        try {
            if (connection != null) {
                this.connection.close();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
