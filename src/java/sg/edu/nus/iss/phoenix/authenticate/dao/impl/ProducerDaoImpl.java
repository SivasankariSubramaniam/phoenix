/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.authenticate.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import sg.edu.nus.iss.phoenix.authenticate.dao.ProducerDao;
import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;
import static sg.edu.nus.iss.phoenix.core.dao.DBConstants.COM_MYSQL_JDBC_DRIVER;
import static sg.edu.nus.iss.phoenix.core.dao.DBConstants.dbPassword;
import static sg.edu.nus.iss.phoenix.core.dao.DBConstants.dbUrl;
import static sg.edu.nus.iss.phoenix.core.dao.DBConstants.dbUserName;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;

/**
 *
 * @author Vikram
 */
public class ProducerDaoImpl implements ProducerDao {

    private static final Logger logger = Logger.getLogger(RoleDaoImpl.class.getName());

    Connection connection;

    /**
     *
     */
    public ProducerDaoImpl() {
        super();
        // TODO Auto-generated constructor stub
        connection = openConnection();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * sg.edu.nus.iss.phoenix.authenticate.dao.impl.UserDao#createValueObject()
     */
    @Override
    public Producer createValueObject() {
        return new Producer();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * sg.edu.nus.iss.phoenix.authenticate.dao.impl.UserDao#getObject(java.sql
     * .Connection, int)
     */
    @Override
    public Producer getObject(String id) throws NotFoundException, SQLException {
        Producer valueObject = null;
        try {
            if (connection == null) {
                connection = openConnection();
            } else if (connection.isClosed()) {
                connection = openConnection();
            }

            valueObject = createValueObject();
            valueObject.setId(id);
        } catch (Exception e) {

        } finally {
            closeConnection();
        }

        //load(valueObject);
        return valueObject;
    }

    @Override
    public List<Producer> loadAll() throws SQLException {
        List<Producer> searchResults = null;

        try {
            if (connection == null) {
                connection = openConnection();
            } else if (connection.isClosed()) {
                connection = openConnection();
            }
        
            String sql = "SELECT * FROM user where role like '%producer%' ORDER BY id ASC ";
            searchResults = listQuery(this.connection.prepareStatement(sql));
        } catch (Exception e) {

        } finally {
            if (connection != null) {
                closeConnection();
            }
        }
        return searchResults;

    }

    /**
     * databaseQuery-method. This method is a helper method for internal use. It
     * will execute all database queries that will return multiple rows. The
     * resultset will be converted to the List of valueObjects. If no rows were
     * found, an empty List will be returned.
     *
     * @param stmt This parameter contains the SQL statement to be excuted.
     * @return
     * @throws java.sql.SQLException
     */
    protected List<Producer> listQuery(PreparedStatement stmt) throws SQLException {

        ArrayList<Producer> searchResults = new ArrayList<>();
        try (ResultSet result = stmt.executeQuery()) {

            while (result.next()) {
                Producer temp = createValueObject();
                temp.setId(result.getString("id"));
                temp.setName(result.getString("name"));
                searchResults.add(temp);
            }

        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return (List<Producer>) searchResults;
    }

    private Connection openConnection() {
        Connection conn = null;
        try {
            Class.forName(COM_MYSQL_JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
        }

        try {
            
            conn = DriverManager.getConnection(
                    dbUrl, dbUserName,
                    dbPassword);
        } catch (SQLException e) {
        }
        return conn;
    }

    private void closeConnection() {
        try {
            if (connection != null) {
                this.connection.close();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
