/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.authenticate.controller;

import at.nocturne.api.Action;
import at.nocturne.api.Perform;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sg.edu.nus.iss.phoenix.authenticate.delegate.AuthenticateDelegate;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;

/**
 *
 * @author boonkui
 */
@Action("login")
public class LoginCmd implements Perform {

  //  @Override

    /**
     *
     * @param path
     * @param req
     * @param resp
     * @return
     * @throws IOException
     * @throws ServletException
     */
    public String perform(String path, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
         // list to contain errors
        List<String> errors = new ArrayList<String>();
        // do validation
        if((req.getParameter("password") == null || req.getParameter("password").isEmpty()) || (req.getParameter("id") == null || req.getParameter("id").isEmpty())) {
            errors.add("Please enter all fields.");
            req.setAttribute("errors", errors);
            return "/pages/login.jsp";
        }
        AuthenticateDelegate ad = new AuthenticateDelegate();
        User user = new User();
        user.setId(req.getParameter("id"));
        user.setPassword(req.getParameter("password"));
        user = ad.validateUserIdPassword(user);
        
        if (null != user) {
            boolean isPasswordMatch = passwordMatch(user, req.getParameter("password"));
            if(!isPasswordMatch) {
                errors.add("Password entered is incorrect.");
                req.setAttribute("errors", errors);
                return "/pages/login.jsp";
            } else {
                req.getSession().setAttribute("user", user);
            return "/pages/home.jsp";
            }
        } else {
            errors.add("User not found.Please re-try with different User ID.");
            req.setAttribute("errors", errors);
            return "/pages/login.jsp";
        }
    }
    
    private boolean passwordMatch(User user, String password) {
        if(password.equals(user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }
}