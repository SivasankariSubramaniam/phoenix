/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.authenticate.entity;

import java.io.Serializable;

/**
 *
 * @author Vikram
 */

/**
 * Presenter Value Object. This class is value object representing database table
 * user This class is intented to be used together with associated Dao object.
 */
public class Producer implements Cloneable, Serializable{
    
    /**
	 * For eclipse based unique identity
	 */
	private static final long serialVersionUID = -3737184031423373200L;
    
        /**
	 * Persistent Instance variables. This data is directly mapped to the
	 * columns of database table.
	 */
	private String id;	
	private String name;
        private String address;
        private String joiningDate;
        
        
        /**
	 * Constructors. The first one takes no arguments and provides the most
	 * simple way to create object instance. The another one takes one argument,
	 * which is the primary key of the corresponding table.
	 */

	public Producer() {

	}
        
    /**
     *
     * @param idIn  id of Producer
     */
    public Producer(String idIn) {

		this.id = idIn;

	}
        
        /**
	 * Get- and Set-methods for persistent variables. The default behaviour does
	 * not make any checks against malformed data, so these might require some
	 * manual additions.
     * @return the id of Producer
	 */

	public String getId() {
		return this.id;
	}

    /**
     *
     * @param idIn  the id of Producer
     */
    public void setId(String idIn) {
		this.id = idIn;
	}
        
    /**
     *
     * @return the name of Producer
     */
    public String getName() {
		return this.name;
	}

    /**
     *
     * @param nameIn    the name of Producer
     */
    public void setName(String nameIn) {
		this.name = nameIn;
	}
        
    /**
     *
     * @return  the address of Producer
     */
    public String getAddress() {
		return this.address;
	}

    /**
     *
     * @param addressIn the address of Producer
     */
    public void setAddress(String addressIn) {
		this.address = addressIn;
	}
        
    /**
     *
     * @return  the date of joining
     */
    public String getJoiningDate() {
		return this.joiningDate;
	}

    /**
     *
     * @param joiningDateIn the date of joining
     */
    public void setJoiningDate(String joiningDateIn) {
		this.joiningDate = joiningDateIn;
	}
        
        /**
	 * setAll allows to set all persistent variables in one method call. This is
	 * useful, when all data is available and it is needed to set the initial
	 * state of this object. Note that this method will directly modify instance
	 * variales, without going trough the individual set-methods.
     * @param idIn  id of producer
     * @param nameIn    name of producer
     * @param joiningDateIn date of joining
     * @param addressIn address of producer
	 */

	public void setAll(String idIn, String nameIn,  String addressIn,
			String joiningDateIn) {
		this.id = idIn;		
		this.name = nameIn;
                this.address = addressIn;
		this.joiningDate=joiningDateIn;
	}
        
        
        /**
	 * hasEqualMapping-method will compare two Role instances and return true if
	 * they contain same values in all persistent instance variables. If
	 * hasEqualMapping returns true, it does not mean the objects are the same
	 * instance. However it does mean that in that moment, they are mapped to
	 * the same row in database.
     * @param valueObject   presenter object to be compared
     * @return  if two object are with same value
	 */
	public boolean hasEqualMapping(Presenter valueObject) {

		if (valueObject.getId() != this.id) {
			return (false);
		}
                
                if (this.name == null) {
			if (valueObject.getName() != null)
				return (false);
		} else if (!this.name.equals(valueObject.getName())) {
			return (false);
		}          
                        
                if (this.address == null) {
			if (valueObject.getAddress() != null)
				return (false);
		} else if (!this.address.equals(valueObject.getAddress())) {
			return (false);
		}           
                       
                if (this.joiningDate == null) {
			if (valueObject.getJoiningDate()!= null)
				return (false);
		} else if (!this.joiningDate.equals(valueObject.getJoiningDate())) {
			return (false);
		}  
          return true;              
	}
        
        
        /**
	 * toString will return String object representing the state of this
	 * valueObject. This is useful during application development, and possibly
	 * when application is writing object states in console logs.
	 */
	public String toString() {
		StringBuffer out = new StringBuffer("toString: ");
		out.append("\nclass Producer, mapping to table user\n");
		out.append("Persistent attributes: \n");
		out.append("id = " + this.id + "\n");
		out.append("name = " + this.name + "\n");
                out.append("address = " + this.address + "\n");
		 out.append("joiningDate = " + this.joiningDate + "\n");
		return out.toString();
	}

        /**
	 * Clone will return identical deep copy of this valueObject. Note, that
	 * this method is different than the clone() which is defined in
	 * java.lang.Object. Here, the returned cloned object will also have all its
	 * attributes cloned.
     * @return cloned Producer object
	 */
	public Object clone() {
		Producer cloned = new Producer();
		if (this.address != null)
			cloned.setAddress(new String(this.address));
		if (this.name != null)
			cloned.setName(new String(this.name));
		if (this.joiningDate != null)
			cloned.setJoiningDate(new String(this.joiningDate));
		return cloned;
	}
}
