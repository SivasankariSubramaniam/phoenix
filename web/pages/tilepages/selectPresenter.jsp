<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
<fmt:setBundle basename="ApplicationResources" />
<title> <fmt:message key="title.selectPresenter"/> </title>
</head>
<body>
        <h1><fmt:message key="label.crudPrgSch.SelectPresenter"/></h1>
        <c:url var="url" scope="page" value="/nocturne/addeditUser">
        	<c:param name="name" value=""/>
                <c:param name="role" value=""/>
                <c:param name="insert" value="true"/>
        </c:url>
       
        <br/><br/>
        <table class="borderAll">
            <tr>
                <th><fmt:message key="label.crudPrgSch.SelectPresenterList"/> </th>
                <th><fmt:message key="label.crudPrgSch.SelectProducerList"/>      </th>
             
                <th>
                    
                </th>
            </tr>
            <tr>
               
                <td>
                    <select id="presenterList">
                        <option>Select Presenter</option>
                        <c:forEach var="presenter" items="${presenters}">
                            <option value="${presenter.id}">${presenter.name}</option>
                        </c:forEach>
                     </select>
                </td>
                 <td>
                    <select id="presenterList">
                        <option>Select Producer</option>
                        <c:forEach var="producer" items="${producers}">
                            <option value="${producer.id}">${producer.name}</option>
                        </c:forEach>
                </select>
                 <td></td>
               
            </tr>
        </table>
</body>
</html>