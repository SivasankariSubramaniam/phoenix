<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <fmt:setBundle basename="ApplicationResources" />

        <title><fmt:message key="title.setupUser" /></title>
        <script type="text/javascript">
            function back()
            {
               location.href = "phoenix/nocturne/manageUser";
            }
      </script>
    </head>
<body>
    <center>
        <c:if test="${fn:length(errors) gt 0}">
            <c:forEach var="error" items="${errors}" varStatus="status">
                <p><font size="3" color="red">${error}</font></p>
            </c:forEach>
        </c:if>
    </center>
    <form action="${pageContext.request.contextPath}/nocturne/enterUserDetails" method=post>
        <center>
            <c:if test="${param['insert']=='true'}">
                <input type="hidden" name="insert" value="true">
            </c:if>
            <c:if test="${param['insert']=='false'}">
                <input type="hidden" name="insert" value="false">
                <input type="hidden" name="password" value="${param['password']}">
            </c:if>
            <table cellpadding=4 cellspacing=2 border=0>
                <c:if test="${param['insert'] == 'true'}">
                <tr>
                    <td><fmt:message key="label.userMaintenance.name" /></td>
                    <td>
                        <input type="text" name="name" value="${param['name']}" size=15 maxlength=20>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="label.userMaintenance.id" /></td>
                    <td>
                        <input type="text" name="id" value="${param['id']}" size=15 maxlength=20>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="label.userMaintenance.role" /></td>
                    <td>
                        <c:forEach var="role" items="${roles}" varStatus="status">
                            <label><input type="checkbox" name="userRole" value="${role.role}"> ${role.role}</label>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td><fmt:message key="label.userMaintenance.password" /></td>
                    <td>
                        <input type="password" name="password" value="${param['password']}" size=15>
                    </td> 
                </tr>
                <tr>
                    <td><fmt:message key="label.userMaintenance.confirmPassword" /></td>   
                    <td>
                        <input type="password" name="confirmPassword" value="${param['confirmPassword']}" size=15>
                    </td>
                </tr>
                <tr>    
                    <input type="hidden" name="ins" value="true" />
                </tr>
                </c:if>     
                <c:if test="${param['insert']=='false'}">
                    <tr>
                        <td><fmt:message key="label.userMaintenance.name" /></td>
                        <td>
                            <input type="text" name="name" value="${param['name']}" size=15 maxlength=20 readonly="readonly">
                        </td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.userMaintenance.id" /></td>
                        <td>
                            <input type="text" name="id" value="${param['id']}" size=15 maxlength=20 readonly="readonly">
                        </td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.userMaintenance.role" /></td>
                        <td>
                            <c:forEach var="role" items="${roles}" varStatus="status">
                                <c:choose>
                                    <c:when test="${fn:contains(userRoles, role.role)}">
                                        <c:if test="${param['view']=='true'}">
                                            <label><input type="checkbox" disabled="disabled" name="userRole" value="${role.role}" checked> ${role.role}</label>
                                        </c:if>
                                        <c:if test="${param['view']=='false'}">
                                            <label><input type="checkbox" name="userRole" value="${role.role}" checked> ${role.role}</label>
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${param['view']=='true'}">
                                            <label><input type="checkbox" disabled="disabled" name="userRole" value="${role.role}"> ${role.role}</label>
                                        </c:if>
                                        <c:if test="${param['view']=='false'}">
                                            <label><input type="checkbox" name="userRole" value="${role.role}"> ${role.role}</label>
                                        </c:if>
                                        
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </td>
                    </tr>
                </c:if>
                    <tr>
                        <c:choose>
                            <c:when test="${param['view'] == 'true'}">
                                <td colspan="3"><button type="button" onclick="back()">Back</button></td>
                            </c:when>
                            <c:otherwise>
                                <td colspan="3"><input type="submit" value="Submit"> <input type="reset" value="Reset"></td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
            </table>
        </center>
    </form>
    </body>
</html>         