<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
        <fmt:setBundle basename="ApplicationResources" />
        <title> <fmt:message key="title.crudPrgSch"/> </title>
    </head>
    <body>
        <h1><fmt:message key="label.crudPrgSch"/></h1>
        <c:url var="url" scope="page" value="/nocturne/addeditPrgSch">
            <c:param name="dateOfProgram" value=""/>
            <c:param name="startTime" value=""/>
            <c:param name="programName" value=""/> 
            <c:param name="producerID" value=""/>
            <c:param name="presenterID" value=""/>       
            <c:param name="insert" value="true"/>
        </c:url>
        <a href="${url}"><fmt:message key="label.crudPrgSch.add"/></a>
        <br/><br/>
        <table class="borderAll">
            <tr>
                <th width="12%"><fmt:message key="label.crudPrgSch.dateOfProgram" /></th>
                <th width="12%"><fmt:message key="label.crudPrgSch.startTime" /></th>
                <th width="21%"><fmt:message key="label.crudPrgSch.programName" /></th>                    
                <th width="15%"><fmt:message key="label.crudPrgSch.presenter" /></th>
                <th width="15%"><fmt:message key="label.crudPrgSch.producer" /></th>                
                <th width="25%"><fmt:message key="label.crudPrgSch.editdelete"/> </th>
            </tr>
            <c:forEach var="crudPrgSch" items="${schPrgs}" varStatus="status">
                <tr class="${status.index%2==0?'even':'odd'}">
                    <td class="nowrap">${crudPrgSch.dateOfProgram}</td>
                    <td class="nowrap">${crudPrgSch.startTime}</td>
                    <td class="nowrap">${crudPrgSch.radioProgram.name}</td>
                    <td class="nowrap">${crudPrgSch.presenterID}</td>
                    <td class="nowrap">${crudPrgSch.producerID}</td>
                    <td class="nowrap">
                        <c:url var="updurl" scope="page" value="/nocturne/addeditPrgSch">
                            <c:param name="dateOfProgram" value="${crudPrgSch.dateOfProgram}"/>
                            <c:param name="startTime" value="${crudPrgSch.startTime}"/>
                            <c:param name="presenterID" value="${crudPrgSch.presenterID}"/>
                            <c:param name="producerID" value="${crudPrgSch.producerID}"/>
                            <c:param name="programName" value="${crudPrgSch.radioProgram.name}"/>
                            <c:param name="insert" value="false"/>
                        </c:url>
                        <a href="${updurl}"><fmt:message key="label.edit"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                        <c:url var="delurl" scope="page" value="/nocturne/deletePrgSch">
                            <c:param name="dateOfProgram" value="${crudPrgSch.dateOfProgram}"/>
                            <c:param name="startTime" value="${crudPrgSch.startTime}"/>
                        </c:url>
                        <a href="${delurl}"><fmt:message key="label.delete"/></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                        <c:url var="copyurl" scope="page" value="/nocturne/copyPrgSch">
                            <c:param name="presenterID" value="${crudPrgSch.presenterID}"/>
                            <c:param name="producerID" value="${crudPrgSch.producerID}"/>
                            <c:param name="programName" value="${crudPrgSch.radioProgram.name}"/>
                            <c:param name="insert" value="copy"/>
                        </c:url>
                        <a href="${copyurl}"><fmt:message key="label.copy"/></a>&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
