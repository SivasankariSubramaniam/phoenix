<%@page import="sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot"%>
<%@page import="sg.edu.nus.iss.phoenix.programschedule.entity.WeeklySchedule.Cell"%>
<%@page import="org.apache.tiles.ognl.RequestScopeNestedObjectExtractor"%>
<%@page import="sg.edu.nus.iss.phoenix.programschedule.entity.WeeklySchedule"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
        <fmt:setBundle basename="ApplicationResources" />
        <title> <fmt:message key="title.crudPrgSch"/> </title>
    </head>
    <body>
        <% WeeklySchedule weekView = (WeeklySchedule) request.getAttribute("weekView");%>
        <form action="${pageContext.request.contextPath}/nocturne/managePrgSch?weeklyView=true"
              method=post>
            Week Start Date&nbsp;&nbsp;<input type="date" value="<%=weekView.getStartDate()%>" name="weeklystartDate"></input>
            <input type="submit" value="Refresh" name="refresh" >
        </form>
        <br />
        <table class="borderAll" cellspacing="0px">

            <tr>
                <th style="border: 1px solid #283747; background-color: #D8D8D8; ">Time</th>
                    <%for (int i = 0; i < 7; i++) {%>
                <th style="border: 1px solid #283747  ;background-color: #D8D8D8; font-size:small"><%=weekView.getDay(i)%><br/><%=weekView.getDate(i)%></th>
                    <%}%>
            </tr>

            <%for (int i = 0; i < weekView.getViewingStartMinute(); i += weekView.getShowIntevalMinute()) {%>
            <tr>
                <td>
                    <c:url var="url" scope="page" value="/nocturne/managePrgSch">
                        <c:param name="startDate" value="<%=weekView.getStartDate()%>"/>
                        <c:param name="viewingStart" value="<%="" + i%>"/>
                    </c:url>
                    <a href="${url}"><%= weekView.getTime(i)%></a>

                </td>
            </tr>
            <%}%>

            <% for (int i = weekView.getViewingStartMinute(); i < weekView.getViewingStartMinute() + weekView.getShowIntevalMinute(); i += weekView.getTimeIntervalMinute()) {%>
            <tr>
                <td style="border: 1px solid #162680;">
                    <%= weekView.getTime(i)%>
                </td>
                <%for (int j = 0; j < 7; j++) {%>
                <% Cell cell = weekView.getCell(i, j); %>
                <% if (cell.isEmpty()) {%>
                <% if (weekView.canAdd(i, j)) {%>
                <td style="border: 1px solid #162680;">
                    <c:url var="urladd" scope="page" value="/nocturne/addeditPrgSch">
                        <%--    <c:param name="starttime" value="<%=weekView.getDate(j) + " " + weekView.getTime(i)%>"/>--%>
                        <c:param name="empty" value="true"/>
                        <c:param name="dateOfProgram" value="<%=weekView.getDate(j)%>"/>
                        <c:param name="startTime" value="<%=weekView.getTime(i)%>"/>
                        <c:param name="programName" value=""/> 
                        <c:param name="producerID" value=""/>
                        <c:param name="presenterID" value=""/>       
                        <c:param name="insert" value="true"/>
                    </c:url>
                    <a href="${urladd}"><img src="${pageContext.request.contextPath}/img/add.png" width="18" height="18" alt="Add Program Slot" border="0"></a>
                </td>
                <%} else {%>
                <td style="border-top: none; border-style: solid #162680;background: #FFCCCC;" >                   
                </td>
                <%}%>

                <% } else {%>

                <% SimpleDateFormat format = new SimpleDateFormat("HH:mm"); %>
                <td style="border-top: 1px; border-right: 1px; border-bottom: 0; border-left: 1px; border-style: solid; border-color: #162680;background: #FFCCCC;">
                    <% ProgramSlot ps = cell.getProgramSlot();%>
                    <%if (cell.isView()) {%>
                    <div>
                        <%=ps.getRadioProgram().getName()%>
                        <%}%>
                        <c:url var="url" scope="page" value="/nocturne/copyPrgSch">                            
                            <c:param name="dateOfProgram" value="<%=ps.getDateOfProgram().toString()%>"/>
                            <c:param name="startTime" value="<%=format.format(ps.getStartTime())%>"/> 
                            <c:param name="presenterID" value="<%=ps.getPresenterID().toString()%>"/>
                            <c:param name="producerID" value="<%=ps.getProducerID().toString()%>"/>
                            <c:param name="programName" value="<%=ps.getRadioProgram().getName()%>"/> 
                            <c:param name="typicalDuration" value="<%=format.format(ps.getRpDuration())%>"/>                              
                        </c:url>
                        <c:if test="${sessionScope.user.roles[0].role == 'manager' || sessionScope.user.roles[0].role == 'producer' || sessionScope.user.roles[0].role == 'presenter'}">
                        <a href="${url}&insert=view"><img src="${pageContext.request.contextPath}/img/view.png" width="18" height="18" title="View Program Slot" border="0"></a>
                        </c:if>
                        <c:if test="${sessionScope.user.roles[0].role == 'manager'}">
                        <a href="${url}&insert=copy"><img src="${pageContext.request.contextPath}/img/copy.png" width="18" height="18" title="Copy Program Slot" border="0"></a>
                        <a href="${url}&insert=false"><img src="${pageContext.request.contextPath}/img/edit.png" width="18" height="18" title="Modify Program Slot" border="0"></a>
                            <c:url var="url" scope="page" value="/nocturne/deletePrgSch">
                                <c:param name="dateOfProgram" value="<%=ps.getDateOfProgram().toString()%>"/>                            
                                <c:param name="startTime" value="<%=format.format(ps.getStartTime())%>"/> 
                            </c:url>
                        <a href="${url}"><img src="${pageContext.request.contextPath}/img/delete.png" width="18" height="18" title="Delete Program Slot" border="0"></a>
                        </c:if>
                    </div>
                </td>
                <% }%>
                <%}%>
            </tr>
            <%}%>
        </table>
    </body>
</html>