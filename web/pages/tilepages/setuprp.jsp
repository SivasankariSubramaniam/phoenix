<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <fmt:setBundle basename="ApplicationResources" />

        <title><fmt:message key="title.setuprp" /></title>
    </head>
    <body>
        <form action="${pageContext.request.contextPath}/nocturne/enterrp" method=post>
            <center>
                <table cellpadding=4 cellspacing=2 border=0>
                    <tr>
                        <td><fmt:message key="label.crudrp.name" /></td>
                        <td><c:if test="${param['insert'] == 'true'}">
                                <input type="text" name="name" value="${param['name']}" size=15
                                       maxlength=20>
                                <input type="hidden" name="ins" value="true" />
                            </c:if> 
                            <c:if test="${param['insert']=='false'}">
                                <input type="text" name="name" value="${param['name']}" size=15
                                       maxlength=20 readonly="readonly">
                                <input type="hidden" name="ins" value="false" />
                            </c:if></td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.crudrp.description" /></td>
                        <td><input type="text" name="description"
                                   value="${param['description']}" size=45 maxlength=20></td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.crudrp.duration" />&nbsp;<fmt:message key="label.crudrp.durationformat" /></td>
                        <td><!--<input type="text" name="typicalDuration"
                                value="${param['typicalDuration']}" size=15 maxlength=20>-->
                            <select name="typicalDuration" id="typicalDuration" >
                                <option>Select Duration in HH:MM</option>                                                
                                <option value="00:00"${'00:00:00' == param['typicalDuration'] ? ' selected' : ''}>00:00</option>
                                <option value="00:30"${'00:30:00' == param['typicalDuration'] ? ' selected' : ''}>00:30</option>
                                <option value="01:00"${'01:00:00' == param['typicalDuration'] ? ' selected' : ''}>01:00</option>
                                <option value="01:30"${'01:30:00' == param['typicalDuration'] ? ' selected' : ''}>01:30</option>
                                <option value="02:00"${'02:00:00' == param['typicalDuration'] ? ' selected' : ''}>02:00</option>
                                <option value="02:30"${'02:30:00' == param['typicalDuration'] ? ' selected' : ''}>02:30</option>
                                <option value="03:00"${'03:00:00' == param['typicalDuration'] ? ' selected' : ''}>03:00</option>
                                <option value="03:30"${'03:30:00' == param['typicalDuration'] ? ' selected' : ''}>03:30</option>
                                <option value="04:00"${'04:00:00' == param['typicalDuration'] ? ' selected' : ''}>04:00</option>
                                <option value="04:30"${'04:30:00' == param['typicalDuration'] ? ' selected' : ''}>04:30</option>
                                <option value="05:00"${'05:00:00' == param['typicalDuration'] ? ' selected' : ''}>05:00</option>
                                <option value="05:30"${'05:30:00' == param['typicalDuration'] ? ' selected' : ''}>05:30</option>
                                <option value="06:00"${'06:00:00' == param['typicalDuration'] ? ' selected' : ''}>06:00</option>
                                <option value="06:30"${'06:30:00' == param['typicalDuration'] ? ' selected' : ''}>06:30</option>
                                <option value="07:00"${'07:00:00' == param['typicalDuration'] ? ' selected' : ''}>07:00</option>
                                <option value="07:30"${'07:30:00' == param['typicalDuration'] ? ' selected' : ''}>07:30</option>
                                <option value="08:00"${'08:00:00' == param['typicalDuration'] ? ' selected' : ''}>08:00</option>
                                <option value="08:30"${'08:30:00' == param['typicalDuration'] ? ' selected' : ''}>07:30</option>
                                <option value="09:00"${'09:00:00' == param['typicalDuration'] ? ' selected' : ''}>09:00</option>
                                <option value="09:30"${'09:30:00' == param['typicalDuration'] ? ' selected' : ''}>09:30</option>
                                <option value="10:00"${'10:00:00' == param['typicalDuration'] ? ' selected' : ''}>10:00</option>
                                <option value="10:30"${'10:30:00' == param['typicalDuration'] ? ' selected' : ''}>10:30</option>
                                <option value="11:00"${'11:00:00' == param['typicalDuration'] ? ' selected' : ''}>11:00</option>
                                <option value="11:30"${'11:30:00' == param['typicalDuration'] ? ' selected' : ''}>11:30</option>
                                <option value="12:00"${'12:00:00' == param['typicalDuration'] ? ' selected' : ''}>12:00</option>
                                <option value="12:30"${'12:30:00' == param['typicalDuration'] ? ' selected' : ''}>12:30</option>
                                <option value="13:00"${'13:00:00' == param['typicalDuration'] ? ' selected' : ''}>13:00</option>
                                <option value="13:30"${'13:30:00' == param['typicalDuration'] ? ' selected' : ''}>13:30</option>
                                <option value="14:00"${'14:00:00' == param['typicalDuration'] ? ' selected' : ''}>14:00</option>
                                <option value="14:30"${'14:30:00' == param['typicalDuration'] ? ' selected' : ''}>14:30</option>
                                <option value="15:00"${'15:00:00' == param['typicalDuration'] ? ' selected' : ''}>15:00</option>
                                <option value="15:30"${'15:30:00' == param['typicalDuration'] ? ' selected' : ''}>15:30</option>
                                <option value="16:00"${'16:00:00' == param['typicalDuration'] ? ' selected' : ''}>16:00</option>
                                <option value="16:30"${'16:30:00' == param['typicalDuration'] ? ' selected' : ''}>16:30</option>
                                <option value="17:00"${'17:00:00' == param['typicalDuration'] ? ' selected' : ''}>17:00</option>
                                <option value="17:30"${'17:30:00' == param['typicalDuration'] ? ' selected' : ''}>17:30</option>
                                <option value="18:00"${'18:00:00' == param['typicalDuration'] ? ' selected' : ''}>18:00</option>
                                <option value="18:30"${'18:30:00' == param['typicalDuration'] ? ' selected' : ''}>18:30</option>
                                <option value="19:00"${'19:00:00' == param['typicalDuration'] ? ' selected' : ''}>19:00</option>
                                <option value="19:30"${'19:30:00' == param['typicalDuration'] ? ' selected' : ''}>19:30</option>
                                <option value="20:00"${'20:00:00' == param['typicalDuration'] ? ' selected' : ''}>20:00</option>
                                <option value="20:30"${'20:30:00' == param['typicalDuration'] ? ' selected' : ''}>20:30</option>
                                <option value="21:00"${'21:00:00' == param['typicalDuration'] ? ' selected' : ''}>21:00</option>
                                <option value="21:30"${'22:30:00' == param['typicalDuration'] ? ' selected' : ''}>21:30</option>
                                <option value="22:00"${'22:00:00' == param['typicalDuration'] ? ' selected' : ''}>22:00</option>
                                <option value="22:30"${'22:30:00' == param['typicalDuration'] ? ' selected' : ''}>22:30</option>
                                <option value="23:00"${'23:00:00' == param['typicalDuration'] ? ' selected' : ''}>23:00</option>
                                <option value="23:30"${'23:30:00' == param['typicalDuration'] ? ' selected' : ''}>23:30</option>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="submit" value="Submit"> <input type="reset" value="Reset"></td>
                    </tr>
                </table>
            </center>

        </form>

    </body>
</html>