<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <fmt:setBundle basename="ApplicationResources" />

        <title><fmt:message key="title.setupPrgSch" /></title>
    </head>
    <body>
        <center>
            <c:if test="${param['insert'] == 'true'}">
                <h1><fmt:message key="title.createPrgSch"/></h1>  
            </c:if>
            <c:if test="${param['insert'] == 'copy'}">
                <h1><fmt:message key="title.copyPrgSch"/></h1>  
            </c:if>
            <c:if test="${param['insert'] == 'view'}">
                <h1><fmt:message key="title.viewPrgSch"/></h1>  
            </c:if>
            <c:if test="${param['insert'] == 'false'}">
                <h1><fmt:message key="title.editPrgSch"/></h1>  
            </c:if>
            <c:if test="${fn:length(errors) gt 0}">
                <c:forEach var="error" items="${errors}" varStatus="status">
                    <p><font size="3" color="red">${error}</font></p>
                        </c:forEach>
                    </c:if>  
        </center>
        <form action="${pageContext.request.contextPath}/nocturne/enterPrgSch" method=post>
            <center>
                <table cellpadding=4 cellspacing=2 border=0>
                    <tr>
                        <td><fmt:message key="label.crudPrgSch.dateOfProgram" /></td>
                        <td><c:if test="${param['insert'] == 'true' || param['insert'] == 'copy'}">
                                <input type="text" name="dateOfProgram" id="dateOfProgram" value="${param['dateOfProgram']}" maxlength=20>
                                <input type="hidden" name="insert" id="insert" value="true" />
                                <fmt:message key="label.crudPrgSch.DateFormat" />
                            </c:if> 
                            <c:if test="${param['insert']=='false'}">
                                <input type="text" name="dateOfProgram" id="dateOfProgram" value="${param['dateOfProgram']}" maxlength=20 disabled="true">
                                <input type="hidden" name="insert"  id="insert" value="false" />
                            </c:if>
                            <c:if test="${param['insert'] == 'view'}">
                                <input type="text" name="dateOfProgram" id="dateOfProgram" value="${param['dateOfProgram']}" maxlength=20 readonly="true">
                                <input type="hidden" name="insert"  id="insert" value="false" />
                            </c:if>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.crudPrgSch.startTime" /></td>
                        <td><c:if test="${param['insert'] == 'true' || param['insert'] == 'copy' }">
                                <select  name="startTime" id="startTime" styleClass="inputField" value="${param['startTime']}" >
                                    <option>-- Select Time --</option>
                                    <option value="00:00"${'00:00' == param['startTime'] ? ' selected' : ''}>00:00</option>
                                    <option value="00:30"${'00:30' == param['startTime'] ? ' selected' : ''}>00:30</option>
                                    <option value="01:00"${'01:00' == param['startTime'] ? ' selected' : ''}>01:00</option>
                                    <option value="01:30"${'01:30' == param['startTime'] ? ' selected' : ''}>01:30</option>
                                    <option value="02:00"${'02:00' == param['startTime'] ? ' selected' : ''}>02:00</option>
                                    <option value="02:30"${'02:30' == param['startTime'] ? ' selected' : ''}>02:30</option>
                                    <option value="03:00"${'03:00' == param['startTime'] ? ' selected' : ''}>03:00</option>
                                    <option value="03:30"${'03:30' == param['startTime'] ? ' selected' : ''}>03:30</option>
                                    <option value="04:00"${'04:00' == param['startTime'] ? ' selected' : ''}>04:00</option>
                                    <option value="04:30"${'04:30' == param['startTime'] ? ' selected' : ''}>04:30</option>
                                    <option value="05:00"${'05:00' == param['startTime'] ? ' selected' : ''}>05:00</option>
                                    <option value="05:30"${'05:30' == param['startTime'] ? ' selected' : ''}>05:30</option>
                                    <option value="06:00"${'06:00' == param['startTime'] ? ' selected' : ''}>06:00</option>
                                    <option value="06:30"${'06:30' == param['startTime'] ? ' selected' : ''}>06:30</option>
                                    <option value="07:00"${'07:00' == param['startTime'] ? ' selected' : ''}>07:00</option>
                                    <option value="07:30"${'07:30' == param['startTime'] ? ' selected' : ''}>07:30</option>
                                    <option value="08:00"${'08:00' == param['startTime'] ? ' selected' : ''}>08:00</option>
                                    <option value="08:30"${'08:30' == param['startTime'] ? ' selected' : ''}>07:30</option>
                                    <option value="09:00"${'09:00' == param['startTime'] ? ' selected' : ''}>09:00</option>
                                    <option value="09:30"${'09:30' == param['startTime'] ? ' selected' : ''}>09:30</option>
                                    <option value="10:00"${'10:00' == param['startTime'] ? ' selected' : ''}>10:00</option>
                                    <option value="10:30"${'10:30' == param['startTime'] ? ' selected' : ''}>10:30</option>
                                    <option value="11:00"${'11:00' == param['startTime'] ? ' selected' : ''}>11:00</option>
                                    <option value="11:30"${'11:30' == param['startTime'] ? ' selected' : ''}>11:30</option>
                                    <option value="12:00"${'12:00' == param['startTime'] ? ' selected' : ''}>12:00</option>
                                    <option value="12:30"${'12:30' == param['startTime'] ? ' selected' : ''}>12:30</option>
                                    <option value="13:00"${'13:00' == param['startTime'] ? ' selected' : ''}>13:00</option>
                                    <option value="13:30"${'13:30' == param['startTime'] ? ' selected' : ''}>13:30</option>
                                    <option value="14:00"${'14:00' == param['startTime'] ? ' selected' : ''}>14:00</option>
                                    <option value="14:30"${'14:30' == param['startTime'] ? ' selected' : ''}>14:30</option>
                                    <option value="15:00"${'15:00' == param['startTime'] ? ' selected' : ''}>15:00</option>
                                    <option value="15:30"${'15:30' == param['startTime'] ? ' selected' : ''}>15:30</option>
                                    <option value="16:00"${'16:00' == param['startTime'] ? ' selected' : ''}>16:00</option>
                                    <option value="16:30"${'16:30' == param['startTime'] ? ' selected' : ''}>16:30</option>
                                    <option value="17:00"${'17:00' == param['startTime'] ? ' selected' : ''}>17:00</option>
                                    <option value="17:30"${'17:30' == param['startTime'] ? ' selected' : ''}>17:30</option>
                                    <option value="18:00"${'18:00' == param['startTime'] ? ' selected' : ''}>18:00</option>
                                    <option value="18:30"${'18:30' == param['startTime'] ? ' selected' : ''}>18:30</option>
                                    <option value="19:00"${'19:00' == param['startTime'] ? ' selected' : ''}>19:00</option>
                                    <option value="19:30"${'19:30' == param['startTime'] ? ' selected' : ''}>19:30</option>
                                    <option value="20:00"${'20:00' == param['startTime'] ? ' selected' : ''}>20:00</option>
                                    <option value="20:30"${'20:30' == param['startTime'] ? ' selected' : ''}>20:30</option>
                                    <option value="21:00"${'21:00' == param['startTime'] ? ' selected' : ''}>21:00</option>
                                    <option value="21:30"${'22:30' == param['startTime'] ? ' selected' : ''}>21:30</option>
                                    <option value="22:00"${'22:00' == param['startTime'] ? ' selected' : ''}>22:00</option>
                                    <option value="22:30"${'22:30' == param['startTime'] ? ' selected' : ''}>22:30</option>
                                    <option value="23:00"${'23:00' == param['startTime'] ? ' selected' : ''}>23:00</option>
                                    <option value="23:30"${'23:30' == param['startTime'] ? ' selected' : ''}>23:30</option>
                                </c:if>
                                    <c:if test="${param['insert'] == 'false'}">
                                        <select  name="startTime" id="startTime" styleClass="inputField" value="${param['startTime']}" disabled="true">
                                            <option>-- Select Time --</option>
                                    <option value="00:00"${'00:00' == param['startTime'] ? ' selected' : ''}>00:00</option>
                                    <option value="00:30"${'00:30' == param['startTime'] ? ' selected' : ''}>00:30</option>
                                    <option value="01:00"${'01:00' == param['startTime'] ? ' selected' : ''}>01:00</option>
                                    <option value="01:30"${'01:30' == param['startTime'] ? ' selected' : ''}>01:30</option>
                                    <option value="02:00"${'02:00' == param['startTime'] ? ' selected' : ''}>02:00</option>
                                    <option value="02:30"${'02:30' == param['startTime'] ? ' selected' : ''}>02:30</option>
                                    <option value="03:00"${'03:00' == param['startTime'] ? ' selected' : ''}>03:00</option>
                                    <option value="03:30"${'03:30' == param['startTime'] ? ' selected' : ''}>03:30</option>
                                    <option value="04:00"${'04:00' == param['startTime'] ? ' selected' : ''}>04:00</option>
                                    <option value="04:30"${'04:30' == param['startTime'] ? ' selected' : ''}>04:30</option>
                                    <option value="05:00"${'05:00' == param['startTime'] ? ' selected' : ''}>05:00</option>
                                    <option value="05:30"${'05:30' == param['startTime'] ? ' selected' : ''}>05:30</option>
                                    <option value="06:00"${'06:00' == param['startTime'] ? ' selected' : ''}>06:00</option>
                                    <option value="06:30"${'06:30' == param['startTime'] ? ' selected' : ''}>06:30</option>
                                    <option value="07:00"${'07:00' == param['startTime'] ? ' selected' : ''}>07:00</option>
                                    <option value="07:30"${'07:30' == param['startTime'] ? ' selected' : ''}>07:30</option>
                                    <option value="08:00"${'08:00' == param['startTime'] ? ' selected' : ''}>08:00</option>
                                    <option value="08:30"${'08:30' == param['startTime'] ? ' selected' : ''}>07:30</option>
                                    <option value="09:00"${'09:00' == param['startTime'] ? ' selected' : ''}>09:00</option>
                                    <option value="09:30"${'09:30' == param['startTime'] ? ' selected' : ''}>09:30</option>
                                    <option value="10:00"${'10:00' == param['startTime'] ? ' selected' : ''}>10:00</option>
                                    <option value="10:30"${'10:30' == param['startTime'] ? ' selected' : ''}>10:30</option>
                                    <option value="11:00"${'11:00' == param['startTime'] ? ' selected' : ''}>11:00</option>
                                    <option value="11:30"${'11:30' == param['startTime'] ? ' selected' : ''}>11:30</option>
                                    <option value="12:00"${'12:00' == param['startTime'] ? ' selected' : ''}>12:00</option>
                                    <option value="12:30"${'12:30' == param['startTime'] ? ' selected' : ''}>12:30</option>
                                    <option value="13:00"${'13:00' == param['startTime'] ? ' selected' : ''}>13:00</option>
                                    <option value="13:30"${'13:30' == param['startTime'] ? ' selected' : ''}>13:30</option>
                                    <option value="14:00"${'14:00' == param['startTime'] ? ' selected' : ''}>14:00</option>
                                    <option value="14:30"${'14:30' == param['startTime'] ? ' selected' : ''}>14:30</option>
                                    <option value="15:00"${'15:00' == param['startTime'] ? ' selected' : ''}>15:00</option>
                                    <option value="15:30"${'15:30' == param['startTime'] ? ' selected' : ''}>15:30</option>
                                    <option value="16:00"${'16:00' == param['startTime'] ? ' selected' : ''}>16:00</option>
                                    <option value="16:30"${'16:30' == param['startTime'] ? ' selected' : ''}>16:30</option>
                                    <option value="17:00"${'17:00' == param['startTime'] ? ' selected' : ''}>17:00</option>
                                    <option value="17:30"${'17:30' == param['startTime'] ? ' selected' : ''}>17:30</option>
                                    <option value="18:00"${'18:00' == param['startTime'] ? ' selected' : ''}>18:00</option>
                                    <option value="18:30"${'18:30' == param['startTime'] ? ' selected' : ''}>18:30</option>
                                    <option value="19:00"${'19:00' == param['startTime'] ? ' selected' : ''}>19:00</option>
                                    <option value="19:30"${'19:30' == param['startTime'] ? ' selected' : ''}>19:30</option>
                                    <option value="20:00"${'20:00' == param['startTime'] ? ' selected' : ''}>20:00</option>
                                    <option value="20:30"${'20:30' == param['startTime'] ? ' selected' : ''}>20:30</option>
                                    <option value="21:00"${'21:00' == param['startTime'] ? ' selected' : ''}>21:00</option>
                                    <option value="21:30"${'22:30' == param['startTime'] ? ' selected' : ''}>21:30</option>
                                    <option value="22:00"${'22:00' == param['startTime'] ? ' selected' : ''}>22:00</option>
                                    <option value="22:30"${'22:30' == param['startTime'] ? ' selected' : ''}>22:30</option>
                                    <option value="23:00"${'23:00' == param['startTime'] ? ' selected' : ''}>23:00</option>
                                    <option value="23:30"${'23:30' == param['startTime'] ? ' selected' : ''}>23:30</option>
                                    </c:if>
                                    <c:if test="${param['insert'] == 'view' }">
                                        <select  name="startTime" id="startTime" styleClass="inputField" value="${param['startTime']}" readonly="true">
                                            <option>-- Select Time --</option>
                                    <option value="00:00"${'00:00' == param['startTime'] ? ' selected' : ''}>00:00</option>
                                    <option value="00:30"${'00:30' == param['startTime'] ? ' selected' : ''}>00:30</option>
                                    <option value="01:00"${'01:00' == param['startTime'] ? ' selected' : ''}>01:00</option>
                                    <option value="01:30"${'01:30' == param['startTime'] ? ' selected' : ''}>01:30</option>
                                    <option value="02:00"${'02:00' == param['startTime'] ? ' selected' : ''}>02:00</option>
                                    <option value="02:30"${'02:30' == param['startTime'] ? ' selected' : ''}>02:30</option>
                                    <option value="03:00"${'03:00' == param['startTime'] ? ' selected' : ''}>03:00</option>
                                    <option value="03:30"${'03:30' == param['startTime'] ? ' selected' : ''}>03:30</option>
                                    <option value="04:00"${'04:00' == param['startTime'] ? ' selected' : ''}>04:00</option>
                                    <option value="04:30"${'04:30' == param['startTime'] ? ' selected' : ''}>04:30</option>
                                    <option value="05:00"${'05:00' == param['startTime'] ? ' selected' : ''}>05:00</option>
                                    <option value="05:30"${'05:30' == param['startTime'] ? ' selected' : ''}>05:30</option>
                                    <option value="06:00"${'06:00' == param['startTime'] ? ' selected' : ''}>06:00</option>
                                    <option value="06:30"${'06:30' == param['startTime'] ? ' selected' : ''}>06:30</option>
                                    <option value="07:00"${'07:00' == param['startTime'] ? ' selected' : ''}>07:00</option>
                                    <option value="07:30"${'07:30' == param['startTime'] ? ' selected' : ''}>07:30</option>
                                    <option value="08:00"${'08:00' == param['startTime'] ? ' selected' : ''}>08:00</option>
                                    <option value="08:30"${'08:30' == param['startTime'] ? ' selected' : ''}>07:30</option>
                                    <option value="09:00"${'09:00' == param['startTime'] ? ' selected' : ''}>09:00</option>
                                    <option value="09:30"${'09:30' == param['startTime'] ? ' selected' : ''}>09:30</option>
                                    <option value="10:00"${'10:00' == param['startTime'] ? ' selected' : ''}>10:00</option>
                                    <option value="10:30"${'10:30' == param['startTime'] ? ' selected' : ''}>10:30</option>
                                    <option value="11:00"${'11:00' == param['startTime'] ? ' selected' : ''}>11:00</option>
                                    <option value="11:30"${'11:30' == param['startTime'] ? ' selected' : ''}>11:30</option>
                                    <option value="12:00"${'12:00' == param['startTime'] ? ' selected' : ''}>12:00</option>
                                    <option value="12:30"${'12:30' == param['startTime'] ? ' selected' : ''}>12:30</option>
                                    <option value="13:00"${'13:00' == param['startTime'] ? ' selected' : ''}>13:00</option>
                                    <option value="13:30"${'13:30' == param['startTime'] ? ' selected' : ''}>13:30</option>
                                    <option value="14:00"${'14:00' == param['startTime'] ? ' selected' : ''}>14:00</option>
                                    <option value="14:30"${'14:30' == param['startTime'] ? ' selected' : ''}>14:30</option>
                                    <option value="15:00"${'15:00' == param['startTime'] ? ' selected' : ''}>15:00</option>
                                    <option value="15:30"${'15:30' == param['startTime'] ? ' selected' : ''}>15:30</option>
                                    <option value="16:00"${'16:00' == param['startTime'] ? ' selected' : ''}>16:00</option>
                                    <option value="16:30"${'16:30' == param['startTime'] ? ' selected' : ''}>16:30</option>
                                    <option value="17:00"${'17:00' == param['startTime'] ? ' selected' : ''}>17:00</option>
                                    <option value="17:30"${'17:30' == param['startTime'] ? ' selected' : ''}>17:30</option>
                                    <option value="18:00"${'18:00' == param['startTime'] ? ' selected' : ''}>18:00</option>
                                    <option value="18:30"${'18:30' == param['startTime'] ? ' selected' : ''}>18:30</option>
                                    <option value="19:00"${'19:00' == param['startTime'] ? ' selected' : ''}>19:00</option>
                                    <option value="19:30"${'19:30' == param['startTime'] ? ' selected' : ''}>19:30</option>
                                    <option value="20:00"${'20:00' == param['startTime'] ? ' selected' : ''}>20:00</option>
                                    <option value="20:30"${'20:30' == param['startTime'] ? ' selected' : ''}>20:30</option>
                                    <option value="21:00"${'21:00' == param['startTime'] ? ' selected' : ''}>21:00</option>
                                    <option value="21:30"${'22:30' == param['startTime'] ? ' selected' : ''}>21:30</option>
                                    <option value="22:00"${'22:00' == param['startTime'] ? ' selected' : ''}>22:00</option>
                                    <option value="22:30"${'22:30' == param['startTime'] ? ' selected' : ''}>22:30</option>
                                    <option value="23:00"${'23:00' == param['startTime'] ? ' selected' : ''}>23:00</option>
                                    <option value="23:30"${'23:30' == param['startTime'] ? ' selected' : ''}>23:30</option>
                                    </c:if>
                                    </td>
                                    </tr>

                                    <tr>
                                        <td><fmt:message key="label.crudPrgSch.programName" /></td>
                                        <td>
                                            <c:if test="${param['insert'] == 'true' || param['insert'] == 'false'}">
                                            <input type="text" id="programName" name="programName" value="${param['programName']}" readonly ="true" />
                                            <c:url var="url" scope="page" value="/nocturne/managerp">                                 
                                                <c:param name="searchrs_setupps" value="true"/>
                                                <c:param name="insert" value="${param['insert']}"/>      
                                            </c:url>
                                            <a href="javascript:setURL('${url}')">Search Radio Program</a>
                                            </c:if>
                                            <c:if test="${param['insert'] == 'copy' || param['insert'] == 'view' }">
                                                <input type="text" id="programName" name="programName" value="${param['programName']}" readonly ="true" />
                                            </c:if>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="label.crudrp.duration" /></td>
                                        <td><input type="text" name="typicalDuration" id="typicalDuration" value="${param['typicalDuration']}" readonly ="true"/> </td>
                                    </tr>

                                    <tr>
                                        <td><fmt:message key="label.crudPrgSch.presenter" /></td>
                                        <td>
                                            <c:if test="${param['insert'] == 'true' || param['insert'] == 'false' }">
                                            <select  name="presenterID" id="presenterID" styleClass="inputField" value="${param['presenterID']}" >
                                                <option>-- Select Presenter --</option>
                                                <c:forEach var="presenter" items="${presenters}">
                                                    <option value="${presenter.id}"${presenter.id == param['presenterID'] ? ' selected' : ''}>${presenter.name}</option>                                                    
                                                </c:forEach>
                                            </select>  
                                            </c:if>
                                            <c:if test="${param['insert'] == 'copy'}">
                                                <select  name="presenterID" id="presenterID" styleClass="inputField" value="${param['presenterID']}" disabled="true">
                                                    <option>-- Select Presenter --</option>
                                                <c:forEach var="presenter" items="${presenters}">
                                                    <option value="${presenter.id}"${presenter.id == param['presenterID'] ? ' selected' : ''}>${presenter.name}</option>                                                    
                                                </c:forEach>
                                            </select>  
                                            </c:if>
                                            <c:if test="${param['insert'] == 'view' }">
                                                <select  name="presenterID" id="presenterID" styleClass="inputField" value="${param['presenterID']}" readonly="true">
                                                    <option>-- Select Presenter --</option>
                                                <c:forEach var="presenter" items="${presenters}">
                                                    <option value="${presenter.id}"${presenter.id == param['presenterID'] ? ' selected' : ''}>${presenter.name}</option>                                                    
                                                </c:forEach>
                                            </select>  
                                            </c:if>
                                        </td>                        
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="label.crudPrgSch.producer" /></td>
                                        <td>
                                            <c:if test="${param['insert'] == 'true' || param['insert'] == 'false' }">
                                            <select name="producerID" id="producerID" styleClass="inputField" value="${param['producerID']}">
                                                <option>-- Select Producer --</option>
                                                <c:forEach var="producer" items="${producers}">
                                                    <option value="${producer.id}"${producer.id == param['producerID'] ? ' selected' : ''}>${producer.name}</option>                                                                                                    
                                                </c:forEach>
                                            </select>
                                            </c:if>
                                            <c:if test="${param['insert'] == 'copy'}">
                                                <select name="producerID" id="producerID" styleClass="inputField" value="${param['producerID']}" disabled="true">
                                                <option>-- Select Producer --</option>
                                                <c:forEach var="producer" items="${producers}">
                                                    <option value="${producer.id}"${producer.id == param['producerID'] ? ' selected' : ''}>${producer.name}</option>                                                                                                    
                                                </c:forEach>
                                            </select>
                                            </c:if>
                                            <c:if test="${param['insert'] == 'view'}">
                                                <select name="producerID" id="producerID" styleClass="inputField" value="${param['producerID']}">
                                                <option>-- Select Producer --</option>
                                                <c:forEach var="producer" items="${producers}">
                                                    <option value="${producer.id}"${producer.id == param['producerID'] ? ' selected' : ''}>${producer.name}</option>                                                                                                    
                                                </c:forEach>
                                            </select>
                                            </c:if>
                                        </td>                        
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <c:if test="${param['insert'] == 'true' || param['insert'] == 'false' || param['insert'] == 'copy'}">
                                                <input type="button" value="Submit" onclick="javascript:setURL('${pageContext.request.contextPath}/nocturne/enterPrgSch?')"> <input type="reset" value="Reset">
                                            </c:if>
                                            <c:if test="${param['insert'] == 'view'}">
                                                <input type="button" value="Back" onclick="javascript:back()">
                                            </c:if>
                                        </td>
                                    </tr>
                                    </table>
                                    </center>
                                    <script>
                                        function setURL(data) {
                                            window.location.href = data + "&insert=" + document.getElementById('insert').value + "&dateOfProgram=" + document.getElementById('dateOfProgram').value + "&startTime=" + document.getElementById('startTime').value
                                                    + "&presenterID=" + document.getElementById('presenterID').value + "&producerID=" + document.getElementById('producerID').value + "&typicalDuration=" + document.getElementById('typicalDuration').value + "&programName=" + document.getElementById('programName').value;
                                        }
                                        function back(){
                                            window.history.back();
                                        }
                                    </script>
                                    </body>
                                    </html>
