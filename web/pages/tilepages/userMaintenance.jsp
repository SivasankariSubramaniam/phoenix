<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
<link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
<fmt:setBundle basename="ApplicationResources" />
<title> <fmt:message key="title.userMaintain"/> </title>
<script type="text/javascript">
   <!--
      function DisplayPasswordResetSucess()
      {
         alert("Password has been set to default");
      }
   //-->
</script>
</head>
<body>
        <h1><fmt:message key="label.userMaintenance"/></h1>
        <c:if test="${fn:contains(sessionScope.user.roles, 'admin')}">
            <c:url var="url" scope="page" value="/nocturne/addeditUser">
                <c:param name="name" value=""/>
                    <c:param name="userRole" value=""/>
                    <c:param name="insert" value="true"/>
                    <c:param name="view" value="false"/>
            </c:url>
            <a href="${url}"><fmt:message key="label.userMaintenance.add"/></a>
        </c:if>
        <br/><br/>
        <table class="borderAll">
            <tr>
                <th><fmt:message key="label.userMaintenance.name"/></th>
                <th><fmt:message key="label.userMaintenance.role"/></th>
                <c:if test="${view == 'true'}"><c:if test="${fn:contains(sessionScope.user.roles, 'producer') || fn:contains(sessionScope.user.roles, 'presenter') || fn:contains(sessionScope.user.roles, 'manager')}"><th><fmt:message key="label.userMaintenance.viewProfile"/></th></c:if></c:if>
                <c:if test="${fn:contains(sessionScope.user.roles, 'producer') || fn:contains(sessionScope.user.roles, 'admin')}"><th><fmt:message key="label.userMaintenance.edit"/></th></c:if>
                <c:if test="${fn:contains(sessionScope.user.roles, 'admin') || fn:contains(sessionScope.user.roles, 'producer')}"><th><fmt:message key="label.userMaintenance.resetPassword"/></th></c:if>
                <c:if test="${fn:contains(sessionScope.user.roles, 'admin')}"><th><fmt:message key="label.userMaintenance.delete"/></th></c:if>
            </tr>
            <c:forEach var="user" items="${users}" varStatus="status">
                <tr class="${status.index%2==0?'even':'odd'}">
                    <td class="nowrap">${user.name}</td>
                    <td class="nowrap">
                        <c:forEach var="role" items="${user.roles}" varStatus="status">
                            <c:out value="${role.role}" escapeXml="false"></c:out>
                            <c:if test="${not status.last}">
                              <c:out value="," escapeXml="false"></c:out> 
                            </c:if>
                        </c:forEach>
                    </td>
                    <c:if test="${view == 'true'}">
                        <c:if test="${fn:contains(sessionScope.user.roles, 'producer') || fn:contains(sessionScope.user.roles, 'manager') || fn:contains(sessionScope.user.roles, 'presenter')}">
                        <td class="nowrap">
                            <c:url var="updurl" scope="page" value="/nocturne/addeditUser">
                                <c:param name="name" value="${user.name}"/>
                                <c:forEach var="role" items="${user.roles}" varStatus="status">
                                    <c:param name="userRole" value="${role.role}"/>
                                </c:forEach>
                                <c:param name="id" value="${user.id}"/>
                                <c:param name="password" value="${user.password}"/>
                                 <c:param name="insert" value="false"/>
                                 <c:param name="view" value="true"/>
                            </c:url>
                            <a href="${updurl}"><fmt:message key="label.userMaintenance.viewProfile"/></a>
                        </td>
                        </c:if>
                    </c:if>
                    <c:if test="${fn:contains(sessionScope.user.roles, 'producer') || fn:contains(sessionScope.user.roles, 'admin')}">
                        <td class="nowrap">
                            <c:url var="updurl" scope="page" value="/nocturne/addeditUser">
                                <c:param name="name" value="${user.name}"/>
                                <c:forEach var="role" items="${user.roles}" varStatus="status">
                                    <c:param name="userRole" value="${role.role}"/>
                                </c:forEach>
                                <c:param name="id" value="${user.id}"/>
                                <c:param name="password" value="${user.password}"/>
                                <c:param name="insert" value="false"/>
                                <c:param name="view" value="false"/>
                            </c:url>
                            <a href="${updurl}"><fmt:message key="label.userMaintenance.edit"/></a>
                        </td>
                    </c:if>
                    <c:if test="${fn:contains(sessionScope.user.roles, 'producer') || fn:contains(sessionScope.user.roles, 'admin')}">
                        <td class="nowrap">
                            <c:url var="resetpassurl" scope="page" value="/nocturne/resetPassword"  >
                                <c:param name="id" value="${user.id}"/>
                            </c:url>
                            <a href="${resetpassurl}"   onclick="DisplayPasswordResetSucess()"><fmt:message key="label.userMaintenance.resetPassword"/></a>
                        </td>
                    </c:if>
                    <c:if test="${fn:contains(sessionScope.user.roles, 'admin')}">
                        <td class="nowrap">
                            <c:url var="delurl" scope="page" value="/nocturne/deleteUser">
                                <c:param name="id" value="${user.id}"/>
                            </c:url>
                            <a href="${delurl}" ><fmt:message key="label.userMaintenance.delete"/></a>
                        </td>
                    </c:if>
                </tr>
            </c:forEach>
        </table>
</body>
</html>