/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.service;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;

/**
 *
 * @author sivasankarisubramaniam,Gandhi
 */
public class ReviewSelectProgramScheduleServiceTest {
    
    ReviewSelectProgramScheduleService service;
    
    /**
     *
     */
    public ReviewSelectProgramScheduleServiceTest() {
    }
   
    
    /**
     *
     */
    @Before
    public void setUp() {
        service=new ReviewSelectProgramScheduleService();
        ScheduleService instance = new ScheduleService();
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
        ProgramSlot prgSch =new ProgramSlot();
        try {
            prgSch.setAll(Calendar.getInstance().getTime(), Integer.toString(1),Integer.toString(2), new RadioProgram(),sdf2.parse("09:00:00"),"pointyhead",sdf2.parse("00:30:00") );
        } catch (ParseException ex) {
            Logger.getLogger(ReviewSelectProgramScheduleServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.processCreate(prgSch);
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        ScheduleService instance = new ScheduleService();
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
        ProgramSlot prgSch =new ProgramSlot();
        Date  startTime;
        try {
            prgSch.setAll(Calendar.getInstance().getTime(), Integer.toString(1),Integer.toString(2), new RadioProgram(),sdf2.parse("09:00:00"),"pointyhead",sdf2.parse("00:30:00") );
            startTime = new SimpleDateFormat("HH:mm").parse("09:00:00");
            instance.processDelete(Calendar.getInstance().getTime(), new Time(startTime.getTime()) );
        } catch (ParseException ex) {
            Logger.getLogger(ReviewSelectProgramScheduleServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     */
    @Test
    public void testloadSchSlot() {
        System.out.println("loadSchSlot");       
        Map<Integer, ArrayList<String>> result= service.loadSchSlot();     
        System.out.println(result.size());
        assert(result.size()>0);
    }
    
    @Test
    public void testloadAll(){
       System.out.println("loadAll");
        List<ProgramSlot> result= service.reviewSelectProgramSchedule();       
        System.out.println(result.size());
        assert(result.size()>0);
           
    }
    @Test
    public void testsearchSchedules(){
       System.out.println("searchSchedules");
//        List<ProgramSlot> result= service.searchSchedules();       
//        System.out.println(result.size());
//        assert(result.size()>0);
//           
    }
    
}
