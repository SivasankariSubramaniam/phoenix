/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.programschedule.service;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sg.edu.nus.iss.phoenix.programschedule.entity.ProgramSlot;
import sg.edu.nus.iss.phoenix.radioprogram.entity.RadioProgram;

/**
 *
 * @author sivasankarisubramaniam
 */
public class ScheduleServiceTest {
    
    /**
     *
     */
    public ScheduleServiceTest() {
    }
    ScheduleService svc=null;
    RadioProgram rp = null;
    ProgramSlot ps = null;
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    
        
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        svc = new ScheduleService();
        rp = new RadioProgram();
        ps = new ProgramSlot();
        Calendar duration = Calendar.getInstance();
        duration.set(2016,10,2,0,0);
        Date startTime = Calendar.getInstance().getTime();
        ps.setAll(Calendar.getInstance().getTime(), "1", "1", rp, startTime , "User1", duration.getTime());
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        svc = null;
    }

    /**
     * Test of processCreate method, of class ScheduleService.
     */
    @Test
    public void testProcessCreate() {

        svc.processCreate(ps);
        assertTrue(true);
    }
     /**
     * Test of processCreate method, of class ScheduleService.
     */
    @Test
    public void testProcessDelete() {
       
        svc.processDelete(ps.getDateOfProgram(), new Time(ps.getStartTime().getTime()));
        assertTrue(true);
    }
}
