/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.authenticate.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author sivasankarisubramaniam
 */
public class LoginCmdTest extends Mockito {
    UserService userService;
    User user;
    
    /**
     *
     */
    public LoginCmdTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        user = new User();
        user.setAll("tony", "password", "tony", "admin");
        userService = new UserService();
        User returnedUser = userService.loadUser(user);
        if(returnedUser == null) {
            userService.processCreate(user);
        }
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        userService.processDelete(user.getId());
        userService = null;
        user = null;
    }

    /**
     * Test of perform method, of class LoginCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerformPasswordNull() throws Exception {
        System.out.println("perform");
        String path = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        when(request.getParameter("password")).thenReturn(null);
        LoginCmd instance = new LoginCmd();
        String expResult = "/pages/login.jsp";
        String result = instance.perform(path, request, response);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of perform method, of class LoginCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerformIDNull() throws Exception {
        System.out.println("perform");
        String path = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        when(request.getParameter("id")).thenReturn(null);
        LoginCmd instance = new LoginCmd();
        String expResult = "/pages/login.jsp";
        String result = instance.perform(path, request, response);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of perform method, of class LoginCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerformPasswordMismatch() throws Exception {
        System.out.println("perform");
        String path = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        when(request.getParameter("id")).thenReturn("tony");
        when(request.getParameter("password")).thenReturn("password3");
        LoginCmd instance = new LoginCmd();
        String expResult = "/pages/login.jsp";
        String result = instance.perform(path, request, response);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of perform method, of class LoginCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerformSuccess() throws Exception {
        System.out.println("perform");
        String path = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = (HttpSession)Mockito.mock(HttpSession.class);
        
        when(request.getParameter("id")).thenReturn("tony");
        when(request.getParameter("password")).thenReturn("password");
        when(request.getSession()).thenReturn(session);
        
        LoginCmd instance = new LoginCmd();
        String expResult = "/pages/home.jsp";
        String result = instance.perform(path, request, response);
        assertEquals(expResult, result);
    }
    
}
