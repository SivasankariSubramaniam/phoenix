/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.authenticate.dao.impl;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.ObjectNotFoundException;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sg.edu.nus.iss.phoenix.authenticate.dao.UserDao;

import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;

import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;

/**
 *
 * @author Vikram
 */
public class PresenterDaoImplTest {
    
    PresenterDaoImpl instance;
    /**
     *
     */
    public PresenterDaoImplTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
       instance= new PresenterDaoImpl();
       User user=new User();
       user.setAll("presenter", "password", "Mr.Presenter", "presenter");
       UserDao userDao=new UserDaoImpl();
       try
       {userDao.create(user);}
       catch(SQLException ex)
       {fail("fail"+ex.toString());}
       
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
       User user=new User();
       user.setAll("presenter", "password", "Mr.Presenter", "presenter");
       UserDao userDao=new UserDaoImpl();
       try
       {userDao.delete(user);}
       catch(SQLException ex)
       {fail("fail"+ex.toString());}
       catch(NotFoundException ex)
       {fail("fail"+ex.toString());}
       
    }
    
    /**
     *
     */
    @Test
    public void testCreateValueObject() {
        System.out.println("createValueObject");        
        Presenter expResult = new Presenter();
        Presenter result = instance.createValueObject();
        assertNotEquals(expResult, result);
     
      
    }
    
    /**
     *
     */
    @Test
    public void testgetObject() {
        System.out.println("getObject");      
        try
        {
        Presenter result = instance.getObject("Abc1");
        assertEquals(result.getId(),"Abc1");
        }
        catch (NotFoundException ex)
        { fail("fail");}
        catch (SQLException ex)
        { fail("fail");}
    }

    /**
     *
     * @throws SQLException
     */
    @Test
        public void testloadAll() throws SQLException {
        System.out.println("loadAll");        
        try
        {
        List<Presenter> result = instance.loadAll(); 
        System.out.println(result.size());
        assert(result.size()>0);
        }   
       catch (SQLException ex)
        { fail("fail");}
            
    }
       
      
    
    
    
}
