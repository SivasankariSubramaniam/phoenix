/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.authenticate.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author sivasankarisubramaniam
 */
public class UserDaoImplTest {
    User user;
    UserService userService;
    
    /**
     *
     */
    public UserDaoImplTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        user = new User();
        user.setAll("testId", "testPassword", "testName", "admin");
        userService = new UserService();
        userService.processCreate(user);
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        userService.processDelete(user.getId());
        userService = null;
        user = null;
    }

    /**
     * Test of load method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    @Test
    public void testLoad() throws Exception {
        System.out.println("load");
        UserDaoImpl instance = new UserDaoImpl();
        User returnedUser = instance.load(user);
        assertSame(user, returnedUser);
    }

    /**
     * Test of loadAll method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    @Test
    public void testLoadAll() throws Exception {
        System.out.println("loadAll");
        UserDaoImpl instance = new UserDaoImpl();
        List<User> result = instance.loadAll();
        assert(result.size() > 0);
    }

    /**
     * Test of create method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    /*@Test
    public void testCreate() throws Exception {
        System.out.println("create");
        User valueObject = new User();
        user.setAll("testNewId", "testNewPassword", "testNewName", "admin");
        UserDaoImpl instance = new UserDaoImpl();
        instance.create(valueObject);
        assertSame(valueObject, instance.load(valueObject));
        
        instance.delete(valueObject);
    }*/

    /**
     * Test of save method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        user.setName("testUpdatedName");
        UserDaoImpl instance = new UserDaoImpl();
        instance.save(user);
        User returnedUser = instance.load(user);
        assertEquals("testUpdatedName", returnedUser.getName());
    }

    /**
     * Test of delete method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    @Test
    public void testDelete() throws Exception {
        System.out.println("delete");
        User valueObject = new User();
        valueObject.setAll("objToDel", "objToDel", "objToDel", "manager");
        UserDaoImpl instance = new UserDaoImpl();
        instance.create(valueObject);
        assertSame(valueObject, instance.load(valueObject));
        instance.delete(valueObject);
        try {
            instance.load(valueObject);
        } catch (NotFoundException e) {
            assertThat(e.getMessage(), is("User Object Not Found!"));
        }
    }

    /**
     * Test of deleteAll method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteAll() throws Exception {
        System.out.println("deleteAll");
        UserDaoImpl instance = new UserDaoImpl();
        instance.deleteAll();
        assertTrue(instance.loadAll().isEmpty());
    }

    /**
     * Test of countAll method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    @Test
    public void testCountAll() throws Exception {
        System.out.println("countAll");
        UserDaoImpl instance = new UserDaoImpl();
        int expResult = 1;
        int result = instance.countAll();
        assertEquals(expResult, result);
    }

    /**
     * Test of searchMatching method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    /*@Test
    public void testSearchMatching_String() throws Exception {
        System.out.println("searchMatching");
        String uid = "";
        UserDaoImpl instance = new UserDaoImpl();
        User expResult = user;
        User result = instance.searchMatching(user.getId());
        assertSame(expResult, result);
    }*/

    /**
     * Test of searchMatching method, of class UserDaoImpl.
     * @throws java.lang.Exception
     */
    /*@Test
    public void testSearchMatching_User() throws Exception {
        System.out.println("searchMatching");
        User valueObject = null;
        UserDaoImpl instance = new UserDaoImpl();
        List<User> expResult = null;
        List<User> result = instance.searchMatching(valueObject);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/


    /**
     * Test of resetPassword method, of class UserDaoImpl.
     */
    @Test
    public void testResetPassword() {
        System.out.println("resetPassword");
        UserDaoImpl instance = new UserDaoImpl();
        try
        {
        boolean result = instance.resetPassword(user);
        assertTrue(result);
        }
        catch (NotFoundException ex)
        { fail("fail");}
        catch (SQLException ex)
        { fail("fail");}
        
        // TODO review the generated test code and remove the default call to fail.
       
    }
    
}
