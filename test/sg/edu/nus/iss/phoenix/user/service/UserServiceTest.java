/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.service;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;

/**
 *
 * @author sivasankarisubramaniam
 */
public class UserServiceTest {
    User user;
    
    /**
     *
     */
    public UserServiceTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        user = new User();
        user.setAll("testId", "testPassword", "testName", "admin");
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        user = null;
    }

    /**
     * Test of processCreate method, of class UserService.
     */
    @Test
    public void testProcessCreate() {
        System.out.println("processCreate");
        UserService instance = new UserService();
        instance.processCreate(user);
        assertSame(user, instance.loadUser(user));
        
        instance.processDelete(user.getId());
    }

    /**
     * Test of processModify method, of class UserService.
     */
    @Test
    public void testProcessModify() {
        System.out.println("processModify");
        UserService instance = new UserService();
        instance.processCreate(user);
        user.setName("newName");
        instance.processModify(user);
        assertEquals("newName", instance.loadUser(user).getName());
        
        instance.processDelete(user.getId());
    }

    /**
     * Test of processDelete method, of class UserService.
     */
    @Test
    public void testProcessDelete() {
        System.out.println("processDelete");
        UserService instance = new UserService();
        instance.processCreate(user);
        assertNotNull(instance.loadUser(user));
        instance.processDelete(user.getId());
        assertNull(instance.loadUser(user));
    }

    /**
     * Test of loadAll method, of class UserService.
     */
    @Test
    public void testLoadAll() {
        System.out.println("loadAll");
        UserService instance = new UserService();
        List<User> expResult = new ArrayList<User>();
        expResult.add(user);
        instance.processCreate(user);
        List<User> result = instance.loadAll();
        assertEquals(expResult.size(), result.size());
        
        instance.processDelete(user.getId());
    }
    
}
