/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.service;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;

/**
 *
 * @author Vikram
 */
public class ReviewSelectProducerServiceTest {
    ReviewSelectProducerService service;
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
       service=new ReviewSelectProducerService();
        UserService instance = new UserService();
        User user=new User();
         user.setAll("producer", "password", "Mr.producer", "producer");
        instance.processCreate(user);
        
        
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        UserService instance = new UserService();
        User user=new User();
        user.setAll("producer", "password", "Mr.producer", "producer");
        instance.processDelete("producer");
    }
    
    @Test
    public void testloadAll(){
       System.out.println("loadAll");       
        List<Producer> result= service.loadAll();     
        System.out.println(result.size());
        assert(result.size()>0);
           
    }
    
}
