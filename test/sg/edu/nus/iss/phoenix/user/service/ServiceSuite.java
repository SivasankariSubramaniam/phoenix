/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.service;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author sivasankarisubramaniam
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({sg.edu.nus.iss.phoenix.user.service.UserServiceTest.class, sg.edu.nus.iss.phoenix.user.service.ReviewSelectPresentorServiceTest.class, sg.edu.nus.iss.phoenix.user.service.ReviewSelectProducerServiceTest.class})
public class ServiceSuite {

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
}
