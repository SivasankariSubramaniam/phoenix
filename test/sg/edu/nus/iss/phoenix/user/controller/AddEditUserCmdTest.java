/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 *
 * @author sivasankarisubramaniam
 */
public class AddEditUserCmdTest {
    
    /**
     *
     */
    public AddEditUserCmdTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of perform method, of class AddEditUserCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerform() throws Exception {
        System.out.println("perform");
        String string = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        AddEditUserCmd instance = new AddEditUserCmd();
        String expResult = "/pages/setupUser.jsp";
        String result = instance.perform(string, request, response);
        assertEquals(expResult, result);
    }
    
}
