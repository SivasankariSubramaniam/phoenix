/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;
import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
/**
 *
 * @author Vikram
 */
public class SelectPresenterProducerCmdTest {
    
     SelectPresenterProducerCmd cmdobj;
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
       cmdobj   = new SelectPresenterProducerCmd();
       
       
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }
    
    /**
     * Test of perform method, of class AddEditUserCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerform() throws Exception {
        System.out.println("perform");
        String path = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);     
        String returnperform=cmdobj.perform(path, request, response);
        System.out.println(returnperform);             
        assert(returnperform=="/pages/selectPresenter.jsp");
        
    }
}
