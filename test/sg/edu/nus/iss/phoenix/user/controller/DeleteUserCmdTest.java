/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author sivasankarisubramaniam
 */
public class DeleteUserCmdTest {
    
    UserService userService; 
    User user;
    
    /**
     *
     */
    public DeleteUserCmdTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        userService = new UserService();
        user = new User();
        user.setAll("tony", "tony", "tony", "admin");
        userService.processCreate(user);
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        if(userService.loadUser(user) != null) {
            userService.processDelete(user.getId());
        }
        
        userService = null;
        user = null;
    }

    /**
     * Test of perform method, of class DeleteUserCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerform() throws Exception {
        System.out.println("perform");
        String path = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        DeleteUserCmd instance = new DeleteUserCmd();
        
        when(request.getParameter("id")).thenReturn("tony");
        
        String expResult = "/pages/userMaintenance.jsp";
        String result = instance.perform(path, request, response);
        assertEquals(expResult, result);
       
    }
    
}
