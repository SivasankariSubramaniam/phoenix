/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;

/**
 *
 * @author sivasankarisubramaniam
 */
public class ManageUserCmdTest extends Mockito {
    
    User newUser;
    
    /**
     *
     */
    public ManageUserCmdTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        newUser = new User();
        newUser.setAll("tony", "password", "tony", "admin");
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of perform method, of class ManageUserCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerform() throws Exception {
        
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = (HttpSession)Mockito.mock(HttpSession.class);
        
        System.out.println("perform");
        String string = "";
        ManageUserCmd instance = new ManageUserCmd();
        when(request.getSession()).thenReturn(session);
        when(request.getSession().getAttribute("user")).thenReturn(newUser);
        String expResult = "/pages/userMaintenance.jsp";
        String result = instance.perform(string, request, response);
        assertEquals(expResult, result);
//        assertEqual()
    }
}
