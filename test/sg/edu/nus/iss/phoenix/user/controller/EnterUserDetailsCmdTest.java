/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author sivasankarisubramaniam
 */
public class EnterUserDetailsCmdTest extends Mockito {
    
    UserService userService;
    User newUser;
    
    /**
     *
     */
    public EnterUserDetailsCmdTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        newUser = new User();
        newUser.setAll("tony", "password", "tony", "admin");
        userService = new UserService();
        User returnedUser = userService.loadUser(newUser);
        if(returnedUser != null) {
            userService.processDelete("tony");
        }
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        userService = null;
        newUser = null;
    }

    /**
     * Test of perform method, of class EnterUserDetailsCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerformSuccessPageCreate() throws Exception {
        System.out.println("testPerformSuccessPageCreate");
        String string = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        when(request.getParameter("name")).thenReturn(newUser.getName());
        when(request.getParameter("password")).thenReturn(newUser.getPassword());
        when(request.getParameter("confirmPassword")).thenReturn(newUser.getPassword());
        when(request.getParameter("id")).thenReturn(newUser.getId());
        when(request.getParameter("insert")).thenReturn("true");
        
        String [] userRoles = new String[1];
        userRoles[0] = ("admin");
        when(request.getParameterValues("userRole")).thenReturn(userRoles);
        
        EnterUserDetailsCmd instance = new EnterUserDetailsCmd();
        String expResult = "/pages/userMaintenance.jsp";
        String result = instance.perform(string, request, response);
        assertEquals(expResult, result);
        
        // delete newly created user
        userService.processDelete("tony");
    }
    
    /**
     * Test of perform method, of class EnterUserDetailsCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerformErrorPageDueToNull() throws Exception {
        System.out.println("testPerformErrorPageDueToNull");
        String string = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        when(request.getParameter("username")).thenReturn(null);
        when(request.getParameter("password")).thenReturn("password");
        when(request.getParameter("confirmPassword")).thenReturn("password");
        when(request.getParameter("admin")).thenReturn("admin");
        
        EnterUserDetailsCmd instance = new EnterUserDetailsCmd();
        String expResult = "/pages/setupUser.jsp";
        String result = instance.perform(string, request, response);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of perform method, of class EnterUserDetailsCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerformErrorPageDueToPasswordMismatch() throws Exception {
        System.out.println("testPerformErrorPageDueToPasswordMismatch");
        String string = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        when(request.getParameter("username")).thenReturn("tony");
        when(request.getParameter("password")).thenReturn("password");
        when(request.getParameter("confirmPassword")).thenReturn("somethingelse");
        when(request.getParameter("admin")).thenReturn("admin");
        
        EnterUserDetailsCmd instance = new EnterUserDetailsCmd();
        String expResult = "/pages/setupUser.jsp";
        String result = instance.perform(string, request, response);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of perform method, of class EnterUserDetailsCmd.
     * @throws java.lang.Exception
     */
    @Test
    public void testPerformSuccessPageModify() throws Exception {
        System.out.println("testPerformSuccessPageModify");
        userService.processCreate(newUser);
        String string = "";
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        when(request.getParameter("name")).thenReturn(newUser.getName());
        when(request.getParameter("password")).thenReturn(newUser.getPassword());
        when(request.getParameter("confirmPassword")).thenReturn(newUser.getPassword());
        when(request.getParameter("id")).thenReturn(newUser.getId());
        when(request.getParameter("insert")).thenReturn("false");
        
        String [] userRoles = new String[1];
        userRoles[0] = ("admin");
        when(request.getParameterValues("userRole")).thenReturn(userRoles);
        
        EnterUserDetailsCmd instance = new EnterUserDetailsCmd();
        String expResult = "/pages/userMaintenance.jsp";
        String result = instance.perform(string, request, response);
        assertEquals(expResult, result);
        
        // delete newly created user
        userService.processDelete("tony");
    }
    
}
