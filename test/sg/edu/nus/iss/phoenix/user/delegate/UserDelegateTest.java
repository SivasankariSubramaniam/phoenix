/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.delegate;

import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.core.exceptions.NotFoundException;

/**
 *
 * @author sivasankarisubramaniam
 */
public class UserDelegateTest {
    User user;
    
    /**
     *
     */
    public UserDelegateTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        user = new User();
        user.setAll("testId", "testPassword", "testName", "admin");
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        user = null;
    }

    /**
     * Test of loadAll method, of class UserDelegate.
     */
    @Test
    public void testLoadAll() {
        System.out.println("loadAll");
        UserDelegate instance = new UserDelegate();
        List<User> expResult = new ArrayList<User>();
        expResult.add(user);
        instance.processCreate(user);
        List<User> result = instance.loadAll();
        assertEquals(expResult.size(), result.size());
        
        instance.processDelete(user.getId());
    }

    /**
     * Test of processCreate method, of class UserDelegate.
     */
    @Test
    public void testProcessCreate() {
        System.out.println("processCreate");
        UserDelegate instance = new UserDelegate();
        instance.processCreate(user);
        assertSame(user, instance.load(user));
        
        instance.processDelete(user.getId());
    }

    /**
     * Test of processModify method, of class UserDelegate.
     */
    @Test
    public void testProcessModify() {
        System.out.println("processModify");
        UserDelegate instance = new UserDelegate();
        instance.processCreate(user);
        user.setName("newName");
        instance.processModify(user);
        assertEquals("newName", instance.load(user).getName());
        
        instance.processDelete(user.getId());
    }

    /**
     * Test of processDelete method, of class UserDelegate.
     */
    @Test
    public void testProcessDelete() {
        System.out.println("processDelete");
        UserDelegate instance = new UserDelegate();
        instance.processCreate(user);
        assertNotNull(instance.load(user));
        instance.processDelete(user.getId());
        assertNull(instance.load(user));
    }
    
}
