/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.edu.nus.iss.phoenix.user.delegate;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sg.edu.nus.iss.phoenix.authenticate.entity.Presenter;
import sg.edu.nus.iss.phoenix.authenticate.entity.Producer;
import sg.edu.nus.iss.phoenix.authenticate.entity.User;
import sg.edu.nus.iss.phoenix.user.service.UserService;

/**
 *
 * @author Vikram
 */
public class ReviewSelectPresentorProducerDelegateTest {
    
    ReviewSelectPresentorProducerDelegate delegate;
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        delegate=new ReviewSelectPresentorProducerDelegate();
        
        UserDelegate instance = new UserDelegate();
        User user=new User();
        user.setAll("presenter", "password", "Mr.Presenter", "presenter");
        instance.processCreate(user);
        
         User user2=new User();
         user2.setAll("producer", "password", "Mr.producer", "producer");
        instance.processCreate(user2);
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        
         UserDelegate instance = new UserDelegate();
        User user=new User();
        user.setAll("presenter", "password", "Mr.Presenter", "presenter");
        instance.processDelete("presenter");
        
         User user2=new User();
         user2.setAll("producer", "password", "Mr.producer", "producer");
         instance.processDelete("producer");
    }

    @Test
    public void testreviewSelectPresenter(){
        System.out.println("reviewSelectPresenter");        
        List<Presenter> result= delegate.reviewSelectPresenter();     
        System.out.println(result.size());
        assert(result.size()>0);

    }
    
     @Test
    public void testreviewSelectProducer(){
        System.out.println("reviewSelectProducer");       
        List<Producer> result= delegate.reviewSelectProducer();     
        System.out.println(result.size());
        assert(result.size()>0);

    }
            
}
